## Data Vault Framework in a box ##

This is a [Vagrant](http://www.vagrantup.com/) configuration for testing the [PDI Data Vault Framework](http://sourceforge.net/projects/pdidatavaultfw/). This framework provides Data Vault loading automation using Pentaho Data Integration.

### Getting started ###

Before you start, make sure you have the following requirements installed:

 * [Git](http://git-scm.com/) 
 * [Virtualbox](https://www.virtualbox.org/)
 * [Vagrant](http://www.vagrantup.com/)

Once these are installed you can start the vm simply by cloning this repository and typing the command `vagrant up`.

    git clone https://bplouvier@bitbucket.org/bplouvier/pdi-dv-vm.git
    cd pdi-dv-vm
    vagrant up
    
### About ###

The virtual machine is a vanilla Ubuntu 32-bit (i386) desktop with a complete development environment for Pentaho Data Integration (including a MySQL database server).

The configuration is based on a Vagrant box using [Virtualbox](https://www.virtualbox.org/) as the virtualization provider. Take a look at the [box definition](https://bitbucket.org/bplouvier/pdi-box) for more details. 

    




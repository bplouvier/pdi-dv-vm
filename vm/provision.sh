#!/bin/bash
HOME=/home/vagrant
WORK_DIR=/vagrant

# Clone PDI datavault framework in home dir
git clone https://bplouvier@bitbucket.org/bplouvier/pdi-dv-framework.git $HOME/datavault

# Restore databases
echo Restoring PDI meta database
mysql -uroot -pvagrant < $WORK_DIR/vm/dumps/pdi_meta_dv_demo.sql
echo Restoring Sakila for data vault sample database
mysql -uroot -pvagrant < $WORK_DIR/vm/dumps/sakila_for_dv_demo.sql
echo Restoring Sakila data vault database
mysql -uroot -pvagrant < $WORK_DIR/vm/dumps/sakila_data_vault.sql

# Move configuration files in place
echo Moving config files in place
mkdir -p $HOME/.kettle
cp $WORK_DIR/vm/config/* $HOME/.kettle


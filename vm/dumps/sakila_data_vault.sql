DROP DATABASE IF EXISTS `sakila_data_vault`;
CREATE DATABASE  IF NOT EXISTS `sakila_data_vault` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sakila_data_vault`;
-- MySQL dump 10.13  Distrib 5.5.24, for Linux (x86_64)
--
-- Host: localhost    Database: sakila_data_vault
-- ------------------------------------------------------
-- Server version	5.5.24-55

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sat_actor`
--

DROP TABLE IF EXISTS `sat_actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sat_actor` (
  `sat_key` int(11) NOT NULL AUTO_INCREMENT,
  `hub_actor_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `load_end_dts` datetime DEFAULT NULL,
  `record_source_id` int(11) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sat_key`),
  UNIQUE KEY `BK` (`hub_actor_id`,`load_dts`),
  KEY `fk_sat_actor_hub_actor1` (`hub_actor_id`),
  CONSTRAINT `fk_sat_actor_hub_actor1` FOREIGN KEY (`hub_actor_id`) REFERENCES `hub_actor` (`hub_actor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=405 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_film_head_actor`
--

DROP TABLE IF EXISTS `link_film_head_actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_film_head_actor` (
  `link_film_head_actor_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_film_id` int(11) NOT NULL,
  `hub_actor_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_film_head_actor_id`),
  UNIQUE KEY `BK` (`hub_film_id`,`hub_actor_id`),
  KEY `fk_link_film_head_actor_hub_film1` (`hub_film_id`),
  KEY `fk_link_film_head_actor_hub_actor1` (`hub_actor_id`),
  CONSTRAINT `fk_link_film_head_actor_hub_actor1` FOREIGN KEY (`hub_actor_id`) REFERENCES `hub_actor` (`hub_actor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_film_head_actor_hub_film1` FOREIGN KEY (`hub_film_id`) REFERENCES `hub_film` (`hub_film_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3948 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_customer_store`
--

DROP TABLE IF EXISTS `link_customer_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_customer_store` (
  `link_customer_store_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_customer_id` int(11) NOT NULL,
  `hub_store_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  `last_seen_dts` datetime DEFAULT NULL,
  PRIMARY KEY (`link_customer_store_id`),
  KEY `fk_link_customer_store_hub_customer1` (`hub_customer_id`),
  KEY `fk_link_customer_store_hub_store1` (`hub_store_id`),
  KEY `BK` (`hub_customer_id`,`hub_store_id`),
  CONSTRAINT `fk_link_customer_store_hub_customer1` FOREIGN KEY (`hub_customer_id`) REFERENCES `hub_customer` (`hub_customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_customer_store_hub_store1` FOREIGN KEY (`hub_store_id`) REFERENCES `hub_store` (`hub_store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_staff_worksin_store`
--

DROP TABLE IF EXISTS `link_staff_worksin_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_staff_worksin_store` (
  `link_staff_worksin_store_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_staff_id` int(11) NOT NULL,
  `hub_store_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_staff_worksin_store_id`),
  UNIQUE KEY `BK` (`hub_staff_id`,`hub_store_id`),
  KEY `fk_link_staff_worksin_store_hub_staff1` (`hub_staff_id`),
  KEY `fk_link_staff_worksin_store_hub_store1` (`hub_store_id`),
  CONSTRAINT `fk_link_staff_worksin_store_hub_staff1` FOREIGN KEY (`hub_staff_id`) REFERENCES `hub_staff` (`hub_staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_staff_worksin_store_hub_store1` FOREIGN KEY (`hub_store_id`) REFERENCES `hub_store` (`hub_store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_language`
--

DROP TABLE IF EXISTS `hub_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_language` (
  `hub_language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` varchar(32) DEFAULT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hub_language_id`) USING BTREE,
  KEY `BK` (`language_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_customer_address`
--

DROP TABLE IF EXISTS `link_customer_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_customer_address` (
  `link_customer_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_customer_id` int(11) NOT NULL,
  `hub_address_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_customer_address_id`) USING BTREE,
  UNIQUE KEY `BK` (`hub_customer_id`,`hub_address_id`) USING BTREE,
  KEY `fk_link_customer_address_hub_address` (`hub_address_id`),
  CONSTRAINT `fk_link_customer_address_hub_address` FOREIGN KEY (`hub_address_id`) REFERENCES `hub_address` (`hub_address_id`),
  CONSTRAINT `fk_link_customer_address_hub_staff` FOREIGN KEY (`hub_customer_id`) REFERENCES `hub_customer` (`hub_customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sat_film`
--

DROP TABLE IF EXISTS `sat_film`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sat_film` (
  `sat_key` int(11) NOT NULL AUTO_INCREMENT,
  `hub_film_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `load_end_dts` datetime DEFAULT NULL,
  `record_source_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `release_year` int(11) DEFAULT NULL,
  `rental_duration` int(11) DEFAULT NULL,
  `rental_rate` decimal(5,2) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `replacement_cost` decimal(5,2) DEFAULT NULL,
  `rating` varchar(10) DEFAULT '?',
  `special_features` varchar(100) DEFAULT '?',
  `last_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sat_key`) USING BTREE,
  KEY `BK` (`hub_film_id`,`load_dts`) USING BTREE,
  CONSTRAINT `fk_sat_film_hub_film1` FOREIGN KEY (`hub_film_id`) REFERENCES `hub_film` (`hub_film_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2002 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_film`
--

DROP TABLE IF EXISTS `hub_film`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_film` (
  `hub_film_id` int(11) NOT NULL AUTO_INCREMENT,
  `film_id` varchar(32) DEFAULT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hub_film_id`),
  KEY `BK` (`film_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4002 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_inventory`
--

DROP TABLE IF EXISTS `hub_inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_inventory` (
  `hub_inventory_id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_id` varchar(32) DEFAULT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hub_inventory_id`),
  UNIQUE KEY `BK` (`inventory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18325 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_customer`
--

DROP TABLE IF EXISTS `hub_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_customer` (
  `hub_customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(32) DEFAULT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hub_customer_id`),
  UNIQUE KEY `BK` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2397 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sat_staff_store_test`
--

DROP TABLE IF EXISTS `sat_staff_store_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sat_staff_store_test` (
  `sat_key` int(11) NOT NULL AUTO_INCREMENT,
  `link_staff_worksin_store_id` int(11) DEFAULT NULL,
  `load_dts` datetime DEFAULT NULL,
  `load_end_dts` datetime DEFAULT NULL,
  `record_source_id` int(11) NOT NULL,
  `username` varchar(16) DEFAULT '?',
  `password` varchar(40) DEFAULT '?',
  PRIMARY KEY (`sat_key`) USING BTREE,
  UNIQUE KEY `BK` (`link_staff_worksin_store_id`,`load_dts`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_category`
--

DROP TABLE IF EXISTS `hub_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_category` (
  `hub_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  `category_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`hub_category_id`),
  KEY `BK` (`category_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sat_address`
--

DROP TABLE IF EXISTS `sat_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sat_address` (
  `sat_key` int(11) NOT NULL AUTO_INCREMENT,
  `hub_address_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `load_end_dts` datetime DEFAULT NULL,
  `record_source_id` int(11) NOT NULL,
  `address` varchar(50) DEFAULT '?',
  `address2` varchar(50) DEFAULT '?',
  `district` varchar(20) DEFAULT '?',
  `postal_code` varchar(10) DEFAULT '?',
  `phone` varchar(20) DEFAULT '?',
  `last_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sat_key`) USING BTREE,
  UNIQUE KEY `BK` (`hub_address_id`,`load_dts`) USING BTREE,
  CONSTRAINT `fk_sat_address_hub_address` FOREIGN KEY (`hub_address_id`) REFERENCES `hub_address` (`hub_address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1208 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_film_actor`
--

DROP TABLE IF EXISTS `link_film_actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_film_actor` (
  `link_film_actor_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_film_id` int(11) NOT NULL,
  `hub_actor_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_film_actor_id`),
  UNIQUE KEY `BK` (`hub_film_id`,`hub_actor_id`),
  KEY `fk_link_film_actor_hub_film1` (`hub_film_id`),
  KEY `fk_link_film_actor_hub_actor1` (`hub_actor_id`),
  CONSTRAINT `fk_link_film_actor_hub_actor1` FOREIGN KEY (`hub_actor_id`) REFERENCES `hub_actor` (`hub_actor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_film_actor_hub_film1` FOREIGN KEY (`hub_film_id`) REFERENCES `hub_film` (`hub_film_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10926 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sat_customer`
--

DROP TABLE IF EXISTS `sat_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sat_customer` (
  `sat_key` int(11) NOT NULL AUTO_INCREMENT,
  `hub_customer_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `load_end_dts` datetime DEFAULT NULL,
  `record_source_id` int(11) NOT NULL,
  `first_name` varchar(45) DEFAULT '?',
  `last_name` varchar(45) DEFAULT '?',
  `email` varchar(50) DEFAULT '?',
  `active` tinyint(4) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sat_key`) USING BTREE,
  UNIQUE KEY `BK` (`hub_customer_id`,`load_dts`) USING BTREE,
  CONSTRAINT `fk_sat_customer_hub_customer` FOREIGN KEY (`hub_customer_id`) REFERENCES `hub_customer` (`hub_customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_address_city`
--

DROP TABLE IF EXISTS `link_address_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_address_city` (
  `link_address_city_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_address_id` int(11) NOT NULL,
  `hub_city_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_address_city_id`) USING BTREE,
  UNIQUE KEY `BK` (`hub_address_id`,`hub_city_id`) USING BTREE,
  KEY `fk_link_address_city_hub_city` (`hub_city_id`),
  CONSTRAINT `fk_link_address_city_hub_address` FOREIGN KEY (`hub_address_id`) REFERENCES `hub_address` (`hub_address_id`),
  CONSTRAINT `fk_link_address_city_hub_city` FOREIGN KEY (`hub_city_id`) REFERENCES `hub_city` (`hub_city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1207 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_film_language`
--

DROP TABLE IF EXISTS `link_film_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_film_language` (
  `link_film_language_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_film_id` int(11) NOT NULL,
  `hub_language_id` int(11) NOT NULL,
  `hub_language_id_original` int(11) DEFAULT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_film_language_id`) USING BTREE,
  UNIQUE KEY `BK` (`hub_film_id`,`hub_language_id`,`hub_language_id_original`) USING BTREE,
  KEY `fk_link_film_language_hub_language` (`hub_language_id`),
  KEY `fk_link_film_language_hub_language_original` (`hub_language_id_original`),
  CONSTRAINT `fk_link_film_language_hub_film` FOREIGN KEY (`hub_film_id`) REFERENCES `hub_film` (`hub_film_id`),
  CONSTRAINT `fk_link_film_language_hub_language` FOREIGN KEY (`hub_language_id`) REFERENCES `hub_language` (`hub_language_id`),
  CONSTRAINT `fk_link_film_language_hub_language_original` FOREIGN KEY (`hub_language_id_original`) REFERENCES `hub_language` (`hub_language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2002 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_actor`
--

DROP TABLE IF EXISTS `hub_actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_actor` (
  `hub_actor_id` int(11) NOT NULL AUTO_INCREMENT,
  `actor_id` varchar(32) DEFAULT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  `last_seen_dts` datetime DEFAULT NULL,
  PRIMARY KEY (`hub_actor_id`),
  KEY `BK` (`actor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=801 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_film_category`
--

DROP TABLE IF EXISTS `link_film_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_film_category` (
  `link_film_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_category_id` int(11) NOT NULL,
  `hub_film_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_film_category_id`),
  UNIQUE KEY `BK` (`hub_category_id`,`hub_film_id`),
  KEY `fk_link_film_category_hub_category1` (`hub_category_id`),
  KEY `fk_link_film_category_hub_film1` (`hub_film_id`),
  CONSTRAINT `fk_link_film_category_hub_category1` FOREIGN KEY (`hub_category_id`) REFERENCES `hub_category` (`hub_category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_film_category_hub_film1` FOREIGN KEY (`hub_film_id`) REFERENCES `hub_film` (`hub_film_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2005 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sat_payment`
--

DROP TABLE IF EXISTS `sat_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sat_payment` (
  `sat_key` int(11) NOT NULL AUTO_INCREMENT,
  `hub_payment_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `load_end_dts` datetime DEFAULT NULL,
  `record_source_id` int(11) DEFAULT NULL,
  `amount` decimal(5,2) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sat_key`),
  KEY `BK` (`load_dts`,`hub_payment_id`),
  KEY `fk_sat_payment_hub_payment1` (`hub_payment_id`),
  CONSTRAINT `fk_sat_payment_hub_payment1` FOREIGN KEY (`hub_payment_id`) REFERENCES `hub_payment` (`hub_payment_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32099 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_rental`
--

DROP TABLE IF EXISTS `hub_rental`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_rental` (
  `hub_rental_id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_id` varchar(32) DEFAULT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hub_rental_id`),
  UNIQUE KEY `BK` (`rental_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64177 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_store`
--

DROP TABLE IF EXISTS `hub_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_store` (
  `hub_store_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` varchar(32) DEFAULT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hub_store_id`),
  UNIQUE KEY `BK` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_payment_rental`
--

DROP TABLE IF EXISTS `link_payment_rental`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_payment_rental` (
  `link_payment_rental_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_rental_id` int(11) DEFAULT NULL,
  `hub_payment_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_payment_rental_id`),
  UNIQUE KEY `BK` (`hub_rental_id`,`hub_payment_id`),
  KEY `fk_link_payment_rental_hub_rental1` (`hub_rental_id`),
  KEY `fk_link_payment_rental_hub_payment1` (`hub_payment_id`),
  CONSTRAINT `fk_link_payment_rental_hub_payment1` FOREIGN KEY (`hub_payment_id`) REFERENCES `hub_payment` (`hub_payment_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_payment_rental_hub_rental1` FOREIGN KEY (`hub_rental_id`) REFERENCES `hub_rental` (`hub_rental_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_rental`
--

DROP TABLE IF EXISTS `link_rental`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_rental` (
  `link_rental_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_rental_id` int(11) NOT NULL,
  `hub_customer_id` int(11) NOT NULL,
  `hub_staff_id` int(11) NOT NULL,
  `hub_inventory_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_rental_id`),
  KEY `fk_link_rental_hub_customer1` (`hub_customer_id`),
  KEY `fk_link_rental_hub_staff1` (`hub_staff_id`),
  KEY `BK` (`hub_customer_id`,`hub_staff_id`,`hub_rental_id`,`hub_inventory_id`),
  KEY `fk_link_rental_hub_rental1` (`hub_rental_id`),
  KEY `fk_link_rental_hub_inventory1` (`hub_inventory_id`),
  CONSTRAINT `fk_link_rental_hub_customer1` FOREIGN KEY (`hub_customer_id`) REFERENCES `hub_customer` (`hub_customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_rental_hub_inventory1` FOREIGN KEY (`hub_inventory_id`) REFERENCES `hub_inventory` (`hub_inventory_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_rental_hub_rental1` FOREIGN KEY (`hub_rental_id`) REFERENCES `hub_rental` (`hub_rental_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_rental_hub_staff1` FOREIGN KEY (`hub_staff_id`) REFERENCES `hub_staff` (`hub_staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32089 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_staff`
--

DROP TABLE IF EXISTS `hub_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_staff` (
  `hub_staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` varchar(32) DEFAULT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hub_staff_id`),
  UNIQUE KEY `BK` (`staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_store_address`
--

DROP TABLE IF EXISTS `link_store_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_store_address` (
  `link_store_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_store_id` int(11) NOT NULL,
  `hub_address_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_store_address_id`) USING BTREE,
  UNIQUE KEY `BK` (`hub_store_id`,`hub_address_id`) USING BTREE,
  KEY `fk_link_store_address_hub_address` (`hub_address_id`),
  CONSTRAINT `fk_link_store_address_hub_address` FOREIGN KEY (`hub_address_id`) REFERENCES `hub_address` (`hub_address_id`),
  CONSTRAINT `fk_link_store_address_hub_store` FOREIGN KEY (`hub_store_id`) REFERENCES `hub_store` (`hub_store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sat_category`
--

DROP TABLE IF EXISTS `sat_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sat_category` (
  `sat_key` int(11) NOT NULL AUTO_INCREMENT,
  `hub_category_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `load_end_dts` datetime DEFAULT NULL,
  `record_source_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT '?',
  `last_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sat_key`) USING BTREE,
  UNIQUE KEY `BK` (`hub_category_id`,`load_dts`) USING BTREE,
  CONSTRAINT `fk_sat_category_hub_category` FOREIGN KEY (`hub_category_id`) REFERENCES `hub_category` (`hub_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sat_language`
--

DROP TABLE IF EXISTS `sat_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sat_language` (
  `sat_key` int(11) NOT NULL AUTO_INCREMENT,
  `hub_language_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `load_end_dts` datetime DEFAULT NULL,
  `record_source_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT '?',
  `last_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sat_key`) USING BTREE,
  UNIQUE KEY `BK` (`hub_language_id`,`load_dts`) USING BTREE,
  CONSTRAINT `fk_sat_language_hub_language` FOREIGN KEY (`hub_language_id`) REFERENCES `hub_language` (`hub_language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sat_city`
--

DROP TABLE IF EXISTS `sat_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sat_city` (
  `sat_key` int(11) NOT NULL AUTO_INCREMENT,
  `hub_city_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `load_end_dts` datetime DEFAULT NULL,
  `record_source_id` int(11) NOT NULL,
  `city` varchar(50) DEFAULT '?',
  `last_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sat_key`) USING BTREE,
  UNIQUE KEY `BK` (`hub_city_id`,`load_dts`) USING BTREE,
  CONSTRAINT `fk_sat_city_hub_city` FOREIGN KEY (`hub_city_id`) REFERENCES `hub_city` (`hub_city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1201 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_payment`
--

DROP TABLE IF EXISTS `hub_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_payment` (
  `hub_payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_id` varchar(32) DEFAULT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hub_payment_id`),
  UNIQUE KEY `BK` (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64197 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sat_store`
--

DROP TABLE IF EXISTS `sat_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sat_store` (
  `sat_key` int(11) NOT NULL AUTO_INCREMENT,
  `hub_store_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `load_end_dts` datetime DEFAULT NULL,
  `record_source_id` int(11) NOT NULL,
  `last_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sat_key`) USING BTREE,
  KEY `BK` (`hub_store_id`,`load_dts`) USING BTREE,
  CONSTRAINT `fk_sat_store_hub_store` FOREIGN KEY (`hub_store_id`) REFERENCES `hub_store` (`hub_store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_inventory`
--

DROP TABLE IF EXISTS `link_inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_inventory` (
  `link_inventory_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_inventory_id` int(11) NOT NULL,
  `hub_film_id` int(11) NOT NULL,
  `hub_store_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_inventory_id`),
  KEY `fk_link_inventory_hub_film1` (`hub_film_id`),
  KEY `fk_link_inventory_hub_store1` (`hub_store_id`),
  KEY `BK` (`hub_film_id`,`hub_store_id`),
  KEY `fk_link_inventory_hub_inventory1` (`hub_inventory_id`),
  CONSTRAINT `fk_link_inventory_hub_film1` FOREIGN KEY (`hub_film_id`) REFERENCES `hub_film` (`hub_film_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_inventory_hub_inventory1` FOREIGN KEY (`hub_inventory_id`) REFERENCES `hub_inventory` (`hub_inventory_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_inventory_hub_store1` FOREIGN KEY (`hub_store_id`) REFERENCES `hub_store` (`hub_store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9163 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_afdeling`
--

DROP TABLE IF EXISTS `hub_afdeling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_afdeling` (
  `hub_afdeling` int(11) NOT NULL AUTO_INCREMENT,
  `afdeling` varchar(16) NOT NULL,
  `record_source_id` int(11) NOT NULL,
  `load_dts` datetime NOT NULL,
  PRIMARY KEY (`hub_afdeling`),
  UNIQUE KEY `hub_afdeling_idx` (`afdeling`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_city`
--

DROP TABLE IF EXISTS `hub_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_city` (
  `hub_city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` varchar(32) DEFAULT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hub_city_id`) USING BTREE,
  KEY `BK` (`city_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2401 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_store_manager`
--

DROP TABLE IF EXISTS `link_store_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_store_manager` (
  `link_store_manager_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_staff_id` int(11) NOT NULL,
  `hub_store_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_store_manager_id`),
  UNIQUE KEY `BK` (`hub_staff_id`,`hub_store_id`),
  KEY `fk_link_store_manager_hub_staff1` (`hub_staff_id`),
  KEY `fk_link_store_manager_hub_store1` (`hub_store_id`),
  CONSTRAINT `fk_link_store_manager_hub_staff1` FOREIGN KEY (`hub_staff_id`) REFERENCES `hub_staff` (`hub_staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_store_manager_hub_store1` FOREIGN KEY (`hub_store_id`) REFERENCES `hub_store` (`hub_store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_city_country`
--

DROP TABLE IF EXISTS `link_city_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_city_country` (
  `link_city_country_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_city_id` int(11) NOT NULL,
  `hub_country_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_city_country_id`) USING BTREE,
  UNIQUE KEY `BK` (`hub_city_id`,`hub_country_id`) USING BTREE,
  KEY `fk_link_city_country_hub_country` (`hub_country_id`),
  CONSTRAINT `fk_link_city_country_hub_city` FOREIGN KEY (`hub_city_id`) REFERENCES `hub_city` (`hub_city_id`),
  CONSTRAINT `fk_link_city_country_hub_country` FOREIGN KEY (`hub_country_id`) REFERENCES `hub_country` (`hub_country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1201 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sat_country`
--

DROP TABLE IF EXISTS `sat_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sat_country` (
  `sat_key` int(11) NOT NULL AUTO_INCREMENT,
  `hub_country_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `load_end_dts` datetime DEFAULT NULL,
  `record_source_id` int(11) NOT NULL,
  `country` varchar(50) DEFAULT '?',
  `last_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sat_key`) USING BTREE,
  UNIQUE KEY `BK` (`hub_country_id`,`load_dts`) USING BTREE,
  CONSTRAINT `fk_sat_country_hub_country` FOREIGN KEY (`hub_country_id`) REFERENCES `hub_country` (`hub_country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_country`
--

DROP TABLE IF EXISTS `hub_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_country` (
  `hub_country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` varchar(32) DEFAULT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hub_country_id`) USING BTREE,
  KEY `BK` (`country_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=437 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_staff_address`
--

DROP TABLE IF EXISTS `link_staff_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_staff_address` (
  `link_staff_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_staff_id` int(11) NOT NULL,
  `hub_address_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_staff_address_id`) USING BTREE,
  UNIQUE KEY `BK` (`hub_staff_id`,`hub_address_id`) USING BTREE,
  KEY `fk_link_staff_address_hub_address` (`hub_address_id`),
  CONSTRAINT `fk_link_staff_address_hub_address` FOREIGN KEY (`hub_address_id`) REFERENCES `hub_address` (`hub_address_id`),
  CONSTRAINT `fk_link_staff_address_hub_staff` FOREIGN KEY (`hub_staff_id`) REFERENCES `hub_staff` (`hub_staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sat_staff`
--

DROP TABLE IF EXISTS `sat_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sat_staff` (
  `sat_key` int(11) NOT NULL AUTO_INCREMENT,
  `hub_staff_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `load_end_dts` datetime DEFAULT NULL,
  `record_source_id` int(11) NOT NULL,
  `first_name` varchar(45) DEFAULT '?',
  `last_name` varchar(45) DEFAULT '?',
  `email` varchar(50) DEFAULT '?',
  `active` tinyint(4) DEFAULT NULL,
  `username` varchar(16) DEFAULT '?',
  `password` varchar(40) DEFAULT '?',
  `last_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sat_key`) USING BTREE,
  UNIQUE KEY `BK` (`hub_staff_id`,`load_dts`) USING BTREE,
  CONSTRAINT `fk_sat_staff_hub_staff` FOREIGN KEY (`hub_staff_id`) REFERENCES `hub_staff` (`hub_staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sat_rental`
--

DROP TABLE IF EXISTS `sat_rental`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sat_rental` (
  `sat_key` int(11) NOT NULL AUTO_INCREMENT,
  `hub_rental_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `load_end_dts` datetime DEFAULT NULL,
  `record_source_id` int(11) DEFAULT NULL,
  `rental_date` datetime DEFAULT NULL,
  `return_date` datetime DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sat_key`),
  KEY `BK` (`load_dts`,`hub_rental_id`),
  KEY `fk_sat_rental_hub_rental1` (`hub_rental_id`),
  CONSTRAINT `fk_sat_rental_hub_rental1` FOREIGN KEY (`hub_rental_id`) REFERENCES `hub_rental` (`hub_rental_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32089 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link_payment`
--

DROP TABLE IF EXISTS `link_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_payment` (
  `link_payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_payment_id` int(11) NOT NULL,
  `hub_customer_id` int(11) NOT NULL,
  `hub_staff_id` int(11) NOT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_payment_id`),
  KEY `fk_link_payment_hub_customer1` (`hub_customer_id`),
  KEY `fk_link_payment_hub_staff1` (`hub_staff_id`),
  KEY `BK` (`hub_customer_id`,`hub_staff_id`,`hub_payment_id`),
  KEY `fk_link_payment_hub_payment1` (`hub_payment_id`),
  CONSTRAINT `fk_link_payment_hub_customer1` FOREIGN KEY (`hub_customer_id`) REFERENCES `hub_customer` (`hub_customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_payment_hub_payment1` FOREIGN KEY (`hub_payment_id`) REFERENCES `hub_payment` (`hub_payment_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_payment_hub_staff1` FOREIGN KEY (`hub_staff_id`) REFERENCES `hub_staff` (`hub_staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32099 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_address`
--

DROP TABLE IF EXISTS `hub_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_address` (
  `hub_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` varchar(32) DEFAULT NULL,
  `load_dts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `record_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hub_address_id`) USING BTREE,
  KEY `BK` (`address_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2413 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hub_opname`
--

DROP TABLE IF EXISTS `hub_opname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_opname` (
  `hub_opname_id` int(11) NOT NULL AUTO_INCREMENT,
  `record_source_id` int(11) NOT NULL,
  `opnamenummer` varchar(32) NOT NULL,
  `load_dts` datetime NOT NULL,
  `record_source_id_1` int(11) NOT NULL,
  PRIMARY KEY (`hub_opname_id`),
  UNIQUE KEY `hub_opname_idx` (`opnamenummer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'sakila_data_vault'
--
/*!50003 DROP PROCEDURE IF EXISTS `prc_create_dv_lkp_index_link` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `prc_create_dv_lkp_index_link`(in p_table_name varchar(128))
BEGIN

SET @findIndex=CONCAT('SHOW INDEXES FROM ',database(),'.',p_table_name,' WHERE key_name = ''','ix_lkp_',p_table_name,'''');

select concat('create unique index ix_lkp_',table_name,' on ',database(),'.',table_name,'(',group_concat(column_name),')')
       as create_index_stmt
from
(
select table_name,ordering,column_name
from   (select table_name,0 as ordering,ordinal_position,column_name
        from   information_schema.columns
        where  table_name = p_table_name
        and    table_schema = (select database())
        and    column_name like 'hub%'
        UNION  ALL
        select table_name,1 as ordering,ordinal_position,column_name
        from   information_schema.columns
        where  table_name = p_table_name
        and    table_schema = (select database())
        and    column_key = 'PRI'
       ) column_names_to_sort
order by table_name,ordering,ordinal_position
) column_names
into @create_index_stmt;

PREPARE findIndex         FROM @findIndex;
PREPARE create_index_stmt FROM @create_index_stmt;
EXECUTE findIndex;
SET @found=FOUND_ROWS();
IF  NOT(@found > 0) THEN EXECUTE create_index_stmt; END IF;

DEALLOCATE PREPARE findIndex;
DEALLOCATE PREPARE create_index_stmt;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `prc_create_dv_lkp_index_links_all` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `prc_create_dv_lkp_index_links_all`()
BEGIN

DECLARE done INT DEFAULT 0;

DECLARE link_table_name VARCHAR(128);
DECLARE cur_table CURSOR FOR 
select table_name
from   information_schema.tables
where  table_name like 'link%'
and    table_name not like 'link%err'
and    table_schema = (select database())
order  by table_name;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

open cur_table;
read_loop: LOOP
    FETCH cur_table INTO link_table_name; 
    IF done THEN LEAVE read_loop; END IF;
    call prc_create_dv_lkp_index_link(link_table_name);
  END LOOP;
  CLOSE cur_table;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed

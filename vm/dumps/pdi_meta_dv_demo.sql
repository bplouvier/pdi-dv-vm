DROP DATABASE IF EXISTS `pdi_meta_dv_demo`;
CREATE DATABASE  IF NOT EXISTS `pdi_meta_dv_demo` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pdi_meta_dv_demo`;
-- MySQL dump 10.13  Distrib 5.5.24, for Linux (x86_64)
--
-- Host: localhost    Database: pdi_meta_dv_demo
-- ------------------------------------------------------
-- Server version	5.5.24-55

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `stg_management_data_vaults`
--

DROP TABLE IF EXISTS `stg_management_data_vaults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stg_management_data_vaults` (
  `id_data_vault` int(11) DEFAULT NULL,
  `data_vault_name` varchar(128) DEFAULT NULL,
  `data_vault_description` varchar(512) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_statuses`
--

DROP TABLE IF EXISTS `ref_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_statuses` (
  `id_status` tinyint(4) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_status_after_i AFTER INSERT ON ref_statuses FOR EACH ROW INSERT 
INTO 
    ref_statuses_hist 
    (
        id_status ,
        hist_date_insert ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        NEW.id_status ,
        NOW() ,
        NEW.description ,
        'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_status_after_u AFTER UPDATE ON ref_statuses FOR EACH ROW INSERT 
INTO 
    ref_statuses_hist 
    (
        id_status ,
        hist_date_insert ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        NEW.id_status ,
        NOW() ,
        NEW.description ,
        'U'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_status_after_d AFTER DELETE ON ref_statuses FOR EACH ROW INSERT 
INTO 
    ref_statuses_hist 
    (
        id_status ,
        hist_date_insert ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        OLD.id_status ,
        NOW() ,
        OLD.description ,
        'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ref_data_vault_hubs`
--

DROP TABLE IF EXISTS `ref_data_vault_hubs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_hubs` (
  `id_data_vault_hub` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_vault` int(11) NOT NULL,
  `hub_name` varchar(128) DEFAULT NULL,
  `hub_key` varchar(128) DEFAULT NULL,
  `hub_business_key` varchar(128) DEFAULT NULL,
  `hub_description` varchar(128) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `ind_last_seen_dts` int(11) DEFAULT '0',
  PRIMARY KEY (`id_data_vault_hub`) USING BTREE,
  UNIQUE KEY `id_data_vault` (`id_data_vault`,`hub_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_dv_hubs_after_i AFTER INSERT ON ref_data_vault_hubs FOR EACH ROW INSERT 
INTO 
    ref_data_vault_hubs_hist 
    (
      id_data_vault_hub
     ,hist_date_insert
     ,id_data_vault 
     ,hub_name
     ,hub_key
     ,hub_business_key
     ,hub_description
     ,ind_current
     ,ind_last_seen_dts
     ,dml_operation
    ) 
    VALUES 
    (
        NEW.id_data_vault_hub,
        NOW(),
        NEW.id_data_vault, 
        NEW.hub_name,
        NEW.hub_key,
        NEW.hub_business_key,
        NEW.hub_description,
        NEW.ind_current,
        NEW.ind_last_seen_dts,
        'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_dv_hubs_after_u AFTER UPDATE ON ref_data_vault_hubs FOR EACH ROW
IF     ifnull(NEW.hub_key,'')           <> ifnull(OLD.hub_key,'')
or     ifnull(NEW.id_data_vault,-1)     <> ifnull(OLD.id_data_vault,-1)
or     ifnull(NEW.hub_name,'')          <> ifnull(OLD.hub_name,'')
or     ifnull(NEW.hub_business_key,'')  <> ifnull(OLD.hub_business_key,'')
or     ifnull(NEW.hub_description,'')   <> ifnull(OLD.hub_description,'')
or     ifnull(NEW.ind_current,-1)       <> ifnull(OLD.ind_current,-1)
or     ifnull(NEW.ind_last_seen_dts,-1) <> ifnull(OLD.ind_last_seen_dts,-1)
THEN
   INSERT INTO 
    ref_data_vault_hubs_hist 
    (
      id_data_vault_hub
     ,hist_date_insert 
     ,id_data_vault 
     ,hub_name
     ,hub_key
     ,hub_business_key
     ,hub_description
     ,ind_current
     ,ind_last_seen_dts
     ,dml_operation
    ) 
    VALUES 
    (
        NEW.id_data_vault_hub,
        NOW(),
        NEW.id_data_vault, 
        NEW.hub_name,
        NEW.hub_key,
        NEW.hub_business_key,
        NEW.hub_description,
        NEW.ind_current,
        NEW.ind_last_seen_dts,
        'U'
    );
END IF */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_dv_hubs_after_d AFTER DELETE ON ref_data_vault_hubs FOR EACH ROW INSERT 
INTO 
    ref_data_vault_hubs_hist 
    (
      id_data_vault_hub
     ,hist_date_insert 
     ,id_data_vault 
     ,hub_name
     ,hub_key
     ,hub_business_key
     ,hub_description
     ,ind_current
     ,ind_last_seen_dts
     ,dml_operation
    ) 
    VALUES 
    (
        OLD.id_data_vault_hub,
        NOW(),
        OLD.id_data_vault, 
        OLD.hub_name,
        OLD.hub_key,
        OLD.hub_business_key,
        OLD.hub_description,
        OLD.ind_current,
        OLD.ind_last_seen_dts,
        'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `inst_run_source_files`
--

DROP TABLE IF EXISTS `inst_run_source_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inst_run_source_files` (
  `id_run` int(11) NOT NULL,
  `id_srcfile` int(11) NOT NULL,
  `source_file` varchar(256) NOT NULL,
  `date_file_last_modified` datetime DEFAULT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime DEFAULT NULL,
  `num_records_file` int(11) DEFAULT NULL,
  `num_records_table` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_run`,`id_srcfile`,`date_start`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_source_tables_hist`
--

DROP TABLE IF EXISTS `ref_source_tables_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_source_tables_hist` (
  `id_srctab` smallint(6) NOT NULL,
  `hist_date_insert` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_srcsys` tinyint(4) NOT NULL,
  `table_name` varchar(256) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `staging_table_name` varchar(128) DEFAULT NULL,
  `dml_operation` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_srctab`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inst_run_parameters`
--

DROP TABLE IF EXISTS `inst_run_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inst_run_parameters` (
  `id_run` int(11) NOT NULL,
  `parameter_naam` varchar(128) NOT NULL,
  `parameter_waarde_varchar` varchar(128) DEFAULT NULL,
  `parameter_waarde_datetime` datetime DEFAULT NULL,
  `parameter_waarde_numeric` decimal(12,2) DEFAULT NULL,
  `etl_id_job` int(11) DEFAULT NULL,
  `etl_id_batch` int(11) DEFAULT NULL,
  `etl_datum_insert` datetime DEFAULT NULL,
  PRIMARY KEY (`id_run`,`parameter_naam`) USING BTREE,
  CONSTRAINT `inst_run_parameters_ibfk_1` FOREIGN KEY (`id_run`) REFERENCES `inst_runs` (`id_run`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_data_vault_hubs_hist`
--

DROP TABLE IF EXISTS `ref_data_vault_hubs_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_hubs_hist` (
  `id_data_vault_hub` int(11) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `id_data_vault` int(11) NOT NULL,
  `hub_name` varchar(128) DEFAULT NULL,
  `hub_key` varchar(128) DEFAULT NULL,
  `hub_business_key` varchar(128) DEFAULT NULL,
  `hub_description` varchar(128) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `dml_operation` char(1) NOT NULL,
  `ind_last_seen_dts` int(11) DEFAULT '0',
  PRIMARY KEY (`id_data_vault_hub`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_management_satellites`
--

DROP TABLE IF EXISTS `stg_management_satellites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stg_management_satellites` (
  `sat_name` varchar(128) DEFAULT NULL,
  `sat_key` varchar(128) DEFAULT NULL,
  `sat_description` varchar(128) DEFAULT NULL,
  `sat_hub` varchar(128) DEFAULT NULL,
  `source_concat` varchar(256) DEFAULT NULL,
  `source_hub_business_key` varchar(128) DEFAULT NULL,
  `attribute_number` int(11) DEFAULT NULL,
  `attribute_source_column` varchar(128) DEFAULT NULL,
  `attribute_target_column` varchar(128) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `sheet_row_number` int(11) DEFAULT NULL,
  `ind_multiactive_extra_key_column` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_source_systems_hist`
--

DROP TABLE IF EXISTS `ref_source_systems_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_source_systems_hist` (
  `id_srcsys` tinyint(4) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `cod_srcsys` varchar(16) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `id_source_connection` int(11) DEFAULT NULL,
  `id_staging_connection` int(11) DEFAULT NULL,
  `dml_operation` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_srcsys`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_management_source_tables`
--

DROP TABLE IF EXISTS `stg_management_source_tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stg_management_source_tables` (
  `source_system` varchar(128) DEFAULT NULL,
  `table_name` varchar(128) DEFAULT NULL,
  `table_description` varchar(128) DEFAULT NULL,
  `staging_table_name` varchar(128) DEFAULT NULL,
  `source_concat` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_data_vault_links`
--

DROP TABLE IF EXISTS `ref_data_vault_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_links` (
  `id_data_vault_link` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_vault` int(11) NOT NULL,
  `link_name` varchar(128) DEFAULT NULL,
  `link_key` varchar(128) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `id_data_vault_hub_1` int(11) DEFAULT NULL,
  `id_data_vault_hub_2` int(11) DEFAULT NULL,
  `id_data_vault_hub_3` int(11) DEFAULT NULL,
  `id_data_vault_hub_4` int(11) DEFAULT NULL,
  `id_data_vault_hub_5` int(11) DEFAULT NULL,
  `id_data_vault_hub_6` int(11) DEFAULT NULL,
  `id_data_vault_hub_7` int(11) DEFAULT NULL,
  `id_data_vault_hub_8` int(11) DEFAULT NULL,
  `id_data_vault_hub_9` int(11) DEFAULT NULL,
  `id_data_vault_hub_10` int(11) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `link_hub_1_key_column` varchar(128) DEFAULT NULL,
  `link_hub_2_key_column` varchar(128) DEFAULT NULL,
  `link_hub_3_key_column` varchar(128) DEFAULT NULL,
  `link_hub_4_key_column` varchar(128) DEFAULT NULL,
  `link_hub_5_key_column` varchar(128) DEFAULT NULL,
  `link_hub_6_key_column` varchar(128) DEFAULT NULL,
  `link_hub_7_key_column` varchar(128) DEFAULT NULL,
  `link_hub_8_key_column` varchar(128) DEFAULT NULL,
  `link_hub_9_key_column` varchar(128) DEFAULT NULL,
  `link_hub_10_key_column` varchar(128) DEFAULT NULL,
  `lnk_cnt_hubs` int(11) DEFAULT NULL,
  `lnk_ind_attributes` int(11) DEFAULT NULL,
  `lnk_bk_columns` varchar(256) DEFAULT NULL,
  `lnk_no_bk_columns` varchar(256) DEFAULT NULL,
  `ind_last_seen_dts` int(11) DEFAULT '0',
  PRIMARY KEY (`id_data_vault_link`) USING BTREE,
  UNIQUE KEY `id_data_vault` (`id_data_vault`,`link_name`) USING BTREE,
  KEY `fk_ref_dv_links_hub_1` (`id_data_vault_hub_1`) USING BTREE,
  KEY `fk_ref_dv_links_hub_2` (`id_data_vault_hub_2`) USING BTREE,
  KEY `fk_ref_dv_links_hub_3` (`id_data_vault_hub_3`) USING BTREE,
  KEY `fk_ref_dv_links_hub_4` (`id_data_vault_hub_4`) USING BTREE,
  KEY `fk_ref_dv_links_hub_5` (`id_data_vault_hub_5`) USING BTREE,
  KEY `fk_ref_dv_links_hub_6` (`id_data_vault_hub_6`) USING BTREE,
  KEY `fk_ref_dv_links_hub_7` (`id_data_vault_hub_7`) USING BTREE,
  KEY `fk_ref_dv_links_hub_8` (`id_data_vault_hub_8`) USING BTREE,
  KEY `fk_ref_dv_links_hub_9` (`id_data_vault_hub_9`) USING BTREE,
  KEY `fk_ref_dv_links_hub_10` (`id_data_vault_hub_10`) USING BTREE,
  CONSTRAINT `fk_ref_dv_links_hub_1` FOREIGN KEY (`id_data_vault_hub_1`) REFERENCES `ref_data_vault_hubs` (`id_data_vault_hub`),
  CONSTRAINT `fk_ref_dv_links_hub_10` FOREIGN KEY (`id_data_vault_hub_10`) REFERENCES `ref_data_vault_hubs` (`id_data_vault_hub`),
  CONSTRAINT `fk_ref_dv_links_hub_2` FOREIGN KEY (`id_data_vault_hub_2`) REFERENCES `ref_data_vault_hubs` (`id_data_vault_hub`),
  CONSTRAINT `fk_ref_dv_links_hub_3` FOREIGN KEY (`id_data_vault_hub_3`) REFERENCES `ref_data_vault_hubs` (`id_data_vault_hub`),
  CONSTRAINT `fk_ref_dv_links_hub_4` FOREIGN KEY (`id_data_vault_hub_4`) REFERENCES `ref_data_vault_hubs` (`id_data_vault_hub`),
  CONSTRAINT `fk_ref_dv_links_hub_5` FOREIGN KEY (`id_data_vault_hub_5`) REFERENCES `ref_data_vault_hubs` (`id_data_vault_hub`),
  CONSTRAINT `fk_ref_dv_links_hub_6` FOREIGN KEY (`id_data_vault_hub_6`) REFERENCES `ref_data_vault_hubs` (`id_data_vault_hub`),
  CONSTRAINT `fk_ref_dv_links_hub_7` FOREIGN KEY (`id_data_vault_hub_7`) REFERENCES `ref_data_vault_hubs` (`id_data_vault_hub`),
  CONSTRAINT `fk_ref_dv_links_hub_8` FOREIGN KEY (`id_data_vault_hub_8`) REFERENCES `ref_data_vault_hubs` (`id_data_vault_hub`),
  CONSTRAINT `fk_ref_dv_links_hub_9` FOREIGN KEY (`id_data_vault_hub_9`) REFERENCES `ref_data_vault_hubs` (`id_data_vault_hub`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_dv_links_after_i AFTER INSERT ON ref_data_vault_links FOR EACH ROW INSERT 
INTO 
    ref_data_vault_links_hist 
    (   
      id_data_vault_link
     ,hist_date_insert
     ,id_data_vault 
     ,link_name
     ,link_key
     ,description          
     ,id_data_vault_hub_1       
     ,id_data_vault_hub_2       
     ,id_data_vault_hub_3       
     ,id_data_vault_hub_4       
     ,id_data_vault_hub_5      
     ,id_data_vault_hub_6      
     ,id_data_vault_hub_7      
     ,id_data_vault_hub_8      
     ,id_data_vault_hub_9      
     ,id_data_vault_hub_10      
     ,ind_current
     ,ind_last_seen_dts
     ,link_hub_1_key_column 
     ,link_hub_2_key_column 
     ,link_hub_3_key_column 
     ,link_hub_4_key_column 
     ,link_hub_5_key_column 
     ,link_hub_6_key_column 
     ,link_hub_7_key_column 
     ,link_hub_8_key_column 
     ,link_hub_9_key_column 
     ,link_hub_10_key_column 
     ,lnk_cnt_hubs
     ,lnk_ind_attributes
     ,dml_operation
     ,lnk_bk_columns
     ,lnk_no_bk_columns
    ) 
    VALUES 
    (
      NEW.id_data_vault_link
     ,NOW()
     ,NEW.id_data_vault 
     ,NEW.link_name
     ,NEW.link_key
     ,NEW.description          
     ,NEW.id_data_vault_hub_1       
     ,NEW.id_data_vault_hub_2       
     ,NEW.id_data_vault_hub_3       
     ,NEW.id_data_vault_hub_4       
     ,NEW.id_data_vault_hub_5      
     ,NEW.id_data_vault_hub_6      
     ,NEW.id_data_vault_hub_7      
     ,NEW.id_data_vault_hub_8      
     ,NEW.id_data_vault_hub_9      
     ,NEW.id_data_vault_hub_10      
     ,NEW.ind_current
     ,NEW.ind_last_seen_dts
     ,NEW.link_hub_1_key_column 
     ,NEW.link_hub_2_key_column 
     ,NEW.link_hub_3_key_column 
     ,NEW.link_hub_4_key_column 
     ,NEW.link_hub_5_key_column 
     ,NEW.link_hub_6_key_column 
     ,NEW.link_hub_7_key_column 
     ,NEW.link_hub_8_key_column 
     ,NEW.link_hub_9_key_column 
     ,NEW.link_hub_10_key_column 
     ,NEW.lnk_cnt_hubs
     ,NEW.lnk_ind_attributes
     ,'I' 
     ,NEW.lnk_bk_columns
     ,NEW.lnk_no_bk_columns
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_dv_links_after_u AFTER UPDATE ON ref_data_vault_links FOR EACH ROW
IF     ifnull(NEW.id_data_vault,-1)        <> ifnull(OLD.id_data_vault,-1)
or     ifnull(NEW.link_name,'')            <> ifnull(OLD.link_name,'')
or     ifnull(NEW.link_key,'')             <> ifnull(OLD.link_key,'')
or     ifnull(NEW.description,'')          <> ifnull(OLD.description,'')
or     ifnull(NEW.id_data_vault_hub_1,-1)  <> ifnull(OLD.id_data_vault_hub_1,-1)
or     ifnull(NEW.id_data_vault_hub_2,-1)  <> ifnull(OLD.id_data_vault_hub_2,-1)
or     ifnull(NEW.id_data_vault_hub_3,-1)  <> ifnull(OLD.id_data_vault_hub_3,-1)
or     ifnull(NEW.id_data_vault_hub_4,-1)  <> ifnull(OLD.id_data_vault_hub_4,-1)
or     ifnull(NEW.id_data_vault_hub_5,-1)  <> ifnull(OLD.id_data_vault_hub_5,-1)
or     ifnull(NEW.id_data_vault_hub_6,-1)  <> ifnull(OLD.id_data_vault_hub_6,-1)
or     ifnull(NEW.id_data_vault_hub_7,-1)  <> ifnull(OLD.id_data_vault_hub_7,-1)
or     ifnull(NEW.id_data_vault_hub_8,-1)  <> ifnull(OLD.id_data_vault_hub_8,-1)
or     ifnull(NEW.id_data_vault_hub_9,-1)  <> ifnull(OLD.id_data_vault_hub_9,-1)
or     ifnull(NEW.id_data_vault_hub_10,-1) <> ifnull(OLD.id_data_vault_hub_10,-1)
or     ifnull(NEW.ind_current,-1)          <> ifnull(OLD.ind_current,-1)
or     ifnull(NEW.ind_last_seen_dts,-1)      <> ifnull(OLD.ind_last_seen_dts,-1)
or     ifnull(NEW.link_hub_1_key_column,'')  <> ifnull(OLD.link_hub_1_key_column,'')
or     ifnull(NEW.link_hub_2_key_column,'')  <> ifnull(OLD.link_hub_2_key_column,'')
or     ifnull(NEW.link_hub_3_key_column,'')  <> ifnull(OLD.link_hub_3_key_column,'')
or     ifnull(NEW.link_hub_4_key_column,'')  <> ifnull(OLD.link_hub_4_key_column,'')
or     ifnull(NEW.link_hub_5_key_column,'')  <> ifnull(OLD.link_hub_5_key_column,'')
or     ifnull(NEW.link_hub_6_key_column,'')  <> ifnull(OLD.link_hub_6_key_column,'')
or     ifnull(NEW.link_hub_7_key_column,'')  <> ifnull(OLD.link_hub_7_key_column,'')
or     ifnull(NEW.link_hub_8_key_column,'')  <> ifnull(OLD.link_hub_8_key_column,'')
or     ifnull(NEW.link_hub_9_key_column,'')  <> ifnull(OLD.link_hub_9_key_column,'')
or     ifnull(NEW.link_hub_10_key_column,'') <> ifnull(OLD.link_hub_10_key_column,'')
or     ifnull(NEW.lnk_cnt_hubs,-1)           <> ifnull(OLD.lnk_cnt_hubs,-1)
or     ifnull(NEW.lnk_ind_attributes,-1)     <> ifnull(OLD.lnk_ind_attributes,-1)
or     ifnull(NEW.lnk_bk_columns,'')         <> ifnull(OLD.lnk_bk_columns,'')
or     ifnull(NEW.lnk_no_bk_columns,'')      <> ifnull(OLD.lnk_no_bk_columns,'')
THEN INSERT 
INTO 
    ref_data_vault_links_hist 
    (   
      id_data_vault_link
     ,hist_date_insert
     ,id_data_vault 
     ,link_name
     ,link_key
     ,description          
     ,id_data_vault_hub_1       
     ,id_data_vault_hub_2       
     ,id_data_vault_hub_3       
     ,id_data_vault_hub_4       
     ,id_data_vault_hub_5      
     ,id_data_vault_hub_6      
     ,id_data_vault_hub_7      
     ,id_data_vault_hub_8      
     ,id_data_vault_hub_9      
     ,id_data_vault_hub_10      
     ,ind_current
     ,ind_last_seen_dts
     ,link_hub_1_key_column 
     ,link_hub_2_key_column 
     ,link_hub_3_key_column 
     ,link_hub_4_key_column 
     ,link_hub_5_key_column 
     ,link_hub_6_key_column 
     ,link_hub_7_key_column 
     ,link_hub_8_key_column 
     ,link_hub_9_key_column 
     ,link_hub_10_key_column 
     ,lnk_cnt_hubs
     ,lnk_ind_attributes
     ,dml_operation
     ,lnk_bk_columns
     ,lnk_no_bk_columns
    ) 
    VALUES 
    (
      NEW.id_data_vault_link
     ,NOW()
     ,NEW.id_data_vault 
     ,NEW.link_name
     ,NEW.link_key
     ,NEW.description          
     ,NEW.id_data_vault_hub_1       
     ,NEW.id_data_vault_hub_2       
     ,NEW.id_data_vault_hub_3       
     ,NEW.id_data_vault_hub_4       
     ,NEW.id_data_vault_hub_5      
     ,NEW.id_data_vault_hub_6      
     ,NEW.id_data_vault_hub_7      
     ,NEW.id_data_vault_hub_8      
     ,NEW.id_data_vault_hub_9      
     ,NEW.id_data_vault_hub_10      
     ,NEW.ind_current
     ,NEW.ind_last_seen_dts
     ,NEW.link_hub_1_key_column 
     ,NEW.link_hub_2_key_column 
     ,NEW.link_hub_3_key_column 
     ,NEW.link_hub_4_key_column 
     ,NEW.link_hub_5_key_column 
     ,NEW.link_hub_6_key_column 
     ,NEW.link_hub_7_key_column 
     ,NEW.link_hub_8_key_column 
     ,NEW.link_hub_9_key_column 
     ,NEW.link_hub_10_key_column 
     ,NEW.lnk_cnt_hubs
     ,NEW.lnk_ind_attributes
     ,'U' 
     ,NEW.lnk_bk_columns
     ,NEW.lnk_no_bk_columns
    );

END IF */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_dv_links_after_d AFTER DELETE ON ref_data_vault_links FOR EACH ROW INSERT 
INTO 
    ref_data_vault_links_hist 
    (   
      id_data_vault_link
     ,hist_date_insert
     ,id_data_vault 
     ,link_name
     ,link_key
     ,description          
     ,id_data_vault_hub_1       
     ,id_data_vault_hub_2       
     ,id_data_vault_hub_3       
     ,id_data_vault_hub_4       
     ,id_data_vault_hub_5      
     ,id_data_vault_hub_6      
     ,id_data_vault_hub_7      
     ,id_data_vault_hub_8      
     ,id_data_vault_hub_9      
     ,id_data_vault_hub_10      
     ,ind_current
     ,ind_last_seen_dts
     ,link_hub_1_key_column 
     ,link_hub_2_key_column 
     ,link_hub_3_key_column 
     ,link_hub_4_key_column 
     ,link_hub_5_key_column 
     ,link_hub_6_key_column 
     ,link_hub_7_key_column 
     ,link_hub_8_key_column 
     ,link_hub_9_key_column 
     ,link_hub_10_key_column 
     ,lnk_cnt_hubs
     ,lnk_ind_attributes
     ,dml_operation
     ,lnk_bk_columns
     ,lnk_no_bk_columns
    ) 
    VALUES 
    (
      OLD.id_data_vault_link
     ,NOW()
     ,OLD.id_data_vault 
     ,OLD.link_name
     ,OLD.link_key
     ,OLD.description          
     ,OLD.id_data_vault_hub_1       
     ,OLD.id_data_vault_hub_2       
     ,OLD.id_data_vault_hub_3       
     ,OLD.id_data_vault_hub_4       
     ,OLD.id_data_vault_hub_5      
     ,OLD.id_data_vault_hub_6      
     ,OLD.id_data_vault_hub_7      
     ,OLD.id_data_vault_hub_8      
     ,OLD.id_data_vault_hub_9      
     ,OLD.id_data_vault_hub_10      
     ,OLD.ind_current
     ,OLD.ind_last_seen_dts
     ,OLD.link_hub_1_key_column 
     ,OLD.link_hub_2_key_column 
     ,OLD.link_hub_3_key_column 
     ,OLD.link_hub_4_key_column 
     ,OLD.link_hub_5_key_column 
     ,OLD.link_hub_6_key_column 
     ,OLD.link_hub_7_key_column 
     ,OLD.link_hub_8_key_column 
     ,OLD.link_hub_9_key_column 
     ,OLD.link_hub_10_key_column 
     ,OLD.lnk_cnt_hubs
     ,OLD.lnk_ind_attributes
     ,'D' 
     ,OLD.lnk_bk_columns
     ,OLD.lnk_no_bk_columns
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ref_connections_hist`
--

DROP TABLE IF EXISTS `ref_connections_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_connections_hist` (
  `id_connection` int(11) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `name` varchar(32) NOT NULL,
  `type` varchar(32) DEFAULT NULL,
  `description` varchar(128) NOT NULL,
  `mysql_host_name` varchar(128) DEFAULT NULL,
  `mysql_database_name` varchar(128) DEFAULT NULL,
  `mysql_port_number` int(11) DEFAULT NULL,
  `mysql_user_name` varchar(128) DEFAULT NULL,
  `mysql_password` varchar(128) DEFAULT NULL,
  `sqlserver_host_name` varchar(128) DEFAULT NULL,
  `sqlserver_database_name` varchar(128) DEFAULT NULL,
  `sqlserver_instance_name` varchar(128) DEFAULT NULL,
  `sqlserver_port_number` int(11) DEFAULT NULL,
  `sqlserver_user_name` varchar(128) DEFAULT NULL,
  `sqlserver_password` varchar(128) DEFAULT NULL,
  `oracle_host_name` varchar(128) DEFAULT NULL,
  `oracle_database_name` varchar(128) DEFAULT NULL,
  `oracle_port_number` int(11) DEFAULT NULL,
  `oracle_user_name` varchar(128) DEFAULT NULL,
  `oracle_password` varchar(128) DEFAULT NULL,
  `postgresql_host_name` varchar(128) DEFAULT NULL,
  `postgresql_database_name` varchar(128) DEFAULT NULL,
  `postgresql_port_number` int(11) DEFAULT NULL,
  `postgresql_user_name` varchar(128) DEFAULT NULL,
  `postgresql_password` varchar(128) DEFAULT NULL,
  `dml_operation` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_connection`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_management_link_satellites`
--

DROP TABLE IF EXISTS `stg_management_link_satellites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stg_management_link_satellites` (
  `sat_name` varchar(128) DEFAULT NULL,
  `sat_key` varchar(128) DEFAULT NULL,
  `sat_description` varchar(128) DEFAULT NULL,
  `sat_link` varchar(128) DEFAULT NULL,
  `source_concat` varchar(256) DEFAULT NULL,
  `source_hub_1_business_key` varchar(128) DEFAULT NULL,
  `source_hub_2_business_key` varchar(128) DEFAULT NULL,
  `source_hub_3_business_key` varchar(128) DEFAULT NULL,
  `source_hub_4_business_key` varchar(128) DEFAULT NULL,
  `source_hub_5_business_key` varchar(128) DEFAULT NULL,
  `source_hub_6_business_key` varchar(128) DEFAULT NULL,
  `source_hub_7_business_key` varchar(128) DEFAULT NULL,
  `source_hub_8_business_key` varchar(128) DEFAULT NULL,
  `source_hub_9_business_key` varchar(128) DEFAULT NULL,
  `source_hub_10_business_key` varchar(128) DEFAULT NULL,
  `source_lnk_key_attribute_1` varchar(128) DEFAULT NULL,
  `source_lnk_key_attribute_2` varchar(128) DEFAULT NULL,
  `source_lnk_key_attribute_3` varchar(128) DEFAULT NULL,
  `source_lnk_key_attribute_4` varchar(128) DEFAULT NULL,
  `source_lnk_key_attribute_5` varchar(128) DEFAULT NULL,
  `attribute_number` int(11) DEFAULT NULL,
  `attribute_source_column` varchar(128) DEFAULT NULL,
  `attribute_target_column` varchar(128) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `sheet_row_number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_runtypes`
--

DROP TABLE IF EXISTS `ref_runtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_runtypes` (
  `id_rtyp` tinyint(4) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_rtyp`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_rtyp_after_i AFTER INSERT ON ref_runtypes FOR EACH ROW INSERT 
INTO 
    ref_runtypes_hist 
    (
        id_rtyp ,
        hist_date_insert ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        NEW.id_rtyp ,
        NOW() ,
        NEW.description ,
        'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_rtyp_after_u AFTER UPDATE ON ref_runtypes FOR EACH ROW INSERT 
INTO 
    ref_runtypes_hist 
    (
        id_rtyp ,
        hist_date_insert ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        NEW.id_rtyp ,
        NOW() ,
        NEW.description ,
        'U'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_rtyp_after_d AFTER DELETE ON ref_runtypes FOR EACH ROW INSERT 
INTO 
    ref_runtypes_hist 
    (
        id_rtyp ,
        hist_date_insert ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        OLD.id_rtyp ,
        NOW() ,
        OLD.description ,
        'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ref_data_vault_hub_satellites`
--

DROP TABLE IF EXISTS `ref_data_vault_hub_satellites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_hub_satellites` (
  `id_data_vault_hub_sat` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_vault_hub` int(11) DEFAULT NULL,
  `sat_name` varchar(128) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `sat_source_hub_business_key` varchar(128) DEFAULT NULL,
  `sat_attributes` varchar(8092) DEFAULT NULL,
  `sat_attributes_concat` varchar(8092) DEFAULT NULL,
  `sat_attributes_concat_dv` varchar(8092) DEFAULT NULL,
  `record_source_id` int(11) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `sat_attributes_dv` varchar(8092) DEFAULT NULL,
  `sat_key` varchar(128) DEFAULT NULL,
  `ind_multiactive_extra_key_column` int(11) DEFAULT '0',
  `sat_multiactive_extra_key_column` varchar(128) DEFAULT NULL,
  `sat_multiactive_extra_key_column_src` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_data_vault_hub_sat`) USING BTREE,
  KEY `fk_ref_dv_hub_satellites_hubs` (`id_data_vault_hub`) USING BTREE,
  KEY `fk_ref_dv_hub_satellites_source_tables` (`record_source_id`) USING BTREE,
  CONSTRAINT `fk_ref_dv_hub_satellites_hubs` FOREIGN KEY (`id_data_vault_hub`) REFERENCES `ref_data_vault_hubs` (`id_data_vault_hub`),
  CONSTRAINT `fk_ref_dv_hub_satellites_source_tables` FOREIGN KEY (`record_source_id`) REFERENCES `ref_source_tables` (`id_srctab`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_ref_dv_hub_sat_after_i` AFTER INSERT ON `ref_data_vault_hub_satellites`
FOR EACH ROW
INSERT 
INTO 
    ref_data_vault_hub_satellites_hist 
    (
      id_data_vault_hub_sat
     ,hist_date_insert 
     ,id_data_vault_hub
     ,sat_name               
     ,description   
     ,sat_source_hub_business_key           
     ,sat_attributes            
     ,sat_attributes_concat     
     ,sat_attributes_concat_dv 
     ,record_source_id         
     ,ind_current
     ,sat_attributes_dv 
     ,sat_key 
     ,ind_multiactive_extra_key_column
     ,sat_multiactive_extra_key_column
     ,sat_multiactive_extra_key_column_src
     ,dml_operation
    ) 
    VALUES 
    (
      NEW.id_data_vault_hub_sat
     ,NOW()
     ,NEW.id_data_vault_hub
     ,NEW.sat_name                
     ,NEW.description              
     ,NEW.sat_source_hub_business_key  
     ,NEW.sat_attributes            
     ,NEW.sat_attributes_concat     
     ,NEW.sat_attributes_concat_dv 
     ,NEW.record_source_id  
     ,NEW.ind_current
     ,NEW.sat_attributes_dv 
     ,NEW.sat_key 
     ,NEW.ind_multiactive_extra_key_column
     ,NEW.sat_multiactive_extra_key_column
     ,NEW.sat_multiactive_extra_key_column_src
     ,'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_ref_dv_hub_sat_after_u` AFTER UPDATE ON `ref_data_vault_hub_satellites`
FOR EACH ROW
IF     ifnull(NEW.id_data_vault_hub,-1)           <> ifnull(OLD.id_data_vault_hub,-1)
or     ifnull(NEW.sat_name,'')                    <> ifnull(OLD.sat_name,'')
or     ifnull(NEW.description,'')                 <> ifnull(OLD.description,'')
or     ifnull(NEW.sat_source_hub_business_key,'') <> ifnull(OLD.sat_source_hub_business_key,'')
or     ifnull(NEW.sat_attributes,'')              <> ifnull(OLD.sat_attributes,'')
or     ifnull(NEW.sat_attributes_concat,'')       <> ifnull(OLD.sat_attributes_concat,'')
or     ifnull(NEW.sat_attributes_concat_dv,'')    <> ifnull(OLD.sat_attributes_concat_dv,'')
or     ifnull(NEW.record_source_id,-1)            <> ifnull(OLD.record_source_id,-1)
or     ifnull(NEW.ind_current,-1)                 <> ifnull(OLD.ind_current,-1)
or     ifnull(NEW.sat_attributes_dv,'')           <> ifnull(OLD.sat_attributes_dv,'')
or     ifnull(NEW.sat_key,'')                     <> ifnull(OLD.sat_key,'')
or     ifnull(NEW.ind_multiactive_extra_key_column,-1)     <> ifnull(OLD.ind_multiactive_extra_key_column,-1)
or     ifnull(NEW.sat_multiactive_extra_key_column,'')     <> ifnull(OLD.sat_multiactive_extra_key_column,'')
or     ifnull(NEW.sat_multiactive_extra_key_column_src,'') <> ifnull(OLD.sat_multiactive_extra_key_column_src,'')
THEN INSERT 
INTO 
    ref_data_vault_hub_satellites_hist 
    (
      id_data_vault_hub_sat
     ,hist_date_insert 
     ,id_data_vault_hub
     ,sat_name              
     ,description    
     ,sat_source_hub_business_key            
     ,sat_attributes            
     ,sat_attributes_concat     
     ,sat_attributes_concat_dv 
     ,record_source_id         
     ,ind_current
     ,sat_attributes_dv 
     ,sat_key 
     ,ind_multiactive_extra_key_column
     ,sat_multiactive_extra_key_column
     ,sat_multiactive_extra_key_column_src
     ,dml_operation
    ) 
    VALUES 
    (
      NEW.id_data_vault_hub_sat
     ,NOW()
     ,NEW.id_data_vault_hub
     ,NEW.sat_name             
     ,NEW.description              
     ,NEW.sat_source_hub_business_key  
     ,NEW.sat_attributes            
     ,NEW.sat_attributes_concat     
     ,NEW.sat_attributes_concat_dv 
     ,NEW.record_source_id  
     ,NEW.ind_current
     ,NEW.sat_attributes_dv 
     ,NEW.sat_key 
     ,NEW.ind_multiactive_extra_key_column
     ,NEW.sat_multiactive_extra_key_column
     ,NEW.sat_multiactive_extra_key_column_src
     , 'U'
    );
END IF */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_ref_dv_hub_sat_after_d` AFTER DELETE ON `ref_data_vault_hub_satellites`
FOR EACH ROW
INSERT 
INTO 
    ref_data_vault_hub_satellites_hist 
    (
      id_data_vault_hub_sat
     ,hist_date_insert 
     ,id_data_vault_hub
     ,sat_name                 
     ,description 
     ,sat_source_hub_business_key               
     ,sat_attributes            
     ,sat_attributes_concat     
     ,sat_attributes_concat_dv 
     ,record_source_id         
     ,ind_current
     ,sat_attributes_dv 
     ,sat_key 
     ,ind_multiactive_extra_key_column
     ,sat_multiactive_extra_key_column
     ,sat_multiactive_extra_key_column_src
     ,dml_operation
    ) 
    VALUES 
    (
      OLD.id_data_vault_hub_sat
     ,NOW()
     ,OLD.id_data_vault_hub
     ,OLD.sat_name                 
     ,OLD.description   
     ,OLD.sat_source_hub_business_key             
     ,OLD.sat_attributes            
     ,OLD.sat_attributes_concat     
     ,OLD.sat_attributes_concat_dv 
     ,OLD.record_source_id  
     ,OLD.ind_current
     ,OLD.sat_attributes_dv 
     ,OLD.sat_key 
     ,OLD.ind_multiactive_extra_key_column
     ,OLD.sat_multiactive_extra_key_column
     ,OLD.sat_multiactive_extra_key_column_src
     , 'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ref_data_vault_links_hist`
--

DROP TABLE IF EXISTS `ref_data_vault_links_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_links_hist` (
  `id_data_vault_link` int(11) NOT NULL,
  `id_data_vault` int(11) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `link_name` varchar(128) DEFAULT NULL,
  `link_key` varchar(128) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `id_data_vault_hub_1` int(11) DEFAULT NULL,
  `id_data_vault_hub_2` int(11) DEFAULT NULL,
  `id_data_vault_hub_3` int(11) DEFAULT NULL,
  `id_data_vault_hub_4` int(11) DEFAULT NULL,
  `id_data_vault_hub_5` int(11) DEFAULT NULL,
  `id_data_vault_hub_6` int(11) DEFAULT NULL,
  `id_data_vault_hub_7` int(11) DEFAULT NULL,
  `id_data_vault_hub_8` int(11) DEFAULT NULL,
  `id_data_vault_hub_9` int(11) DEFAULT NULL,
  `id_data_vault_hub_10` int(11) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `link_hub_1_key_column` varchar(128) DEFAULT NULL,
  `link_hub_2_key_column` varchar(128) DEFAULT NULL,
  `link_hub_3_key_column` varchar(128) DEFAULT NULL,
  `link_hub_4_key_column` varchar(128) DEFAULT NULL,
  `link_hub_5_key_column` varchar(128) DEFAULT NULL,
  `link_hub_6_key_column` varchar(128) DEFAULT NULL,
  `link_hub_7_key_column` varchar(128) DEFAULT NULL,
  `link_hub_8_key_column` varchar(128) DEFAULT NULL,
  `link_hub_9_key_column` varchar(128) DEFAULT NULL,
  `link_hub_10_key_column` varchar(128) DEFAULT NULL,
  `lnk_cnt_hubs` int(11) DEFAULT NULL,
  `lnk_ind_attributes` int(11) DEFAULT NULL,
  `dml_operation` char(1) NOT NULL,
  `lnk_bk_columns` varchar(256) DEFAULT NULL,
  `lnk_no_bk_columns` varchar(256) DEFAULT NULL,
  `ind_last_seen_dts` int(11) DEFAULT '0',
  PRIMARY KEY (`id_data_vault_link`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_management_hubs`
--

DROP TABLE IF EXISTS `stg_management_hubs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stg_management_hubs` (
  `hub_name` varchar(128) DEFAULT NULL,
  `hub_description` varchar(256) DEFAULT NULL,
  `hub_key` varchar(128) DEFAULT NULL,
  `hub_business_key` varchar(128) DEFAULT NULL,
  `hub_source` varchar(256) DEFAULT NULL,
  `hub_source_business_key` varchar(128) DEFAULT NULL,
  `hub_source_order` int(11) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `ind_last_seen_dts` int(11) DEFAULT NULL,
  `ind_status_sat` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_management_dv_design_errors`
--

DROP TABLE IF EXISTS `stg_management_dv_design_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stg_management_dv_design_errors` (
  `sheet` varchar(128) DEFAULT NULL,
  `message` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_management_source_systems`
--

DROP TABLE IF EXISTS `stg_management_source_systems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stg_management_source_systems` (
  `id_srcsys` int(11) DEFAULT NULL,
  `cod_srcsys` varchar(16) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `source_connection` varchar(128) DEFAULT NULL,
  `staging_connection` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `vw_stg_management_link_satellites_all_columns_filled`
--

DROP TABLE IF EXISTS `vw_stg_management_link_satellites_all_columns_filled`;
/*!50001 DROP VIEW IF EXISTS `vw_stg_management_link_satellites_all_columns_filled`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_stg_management_link_satellites_all_columns_filled` (
  `sat_name` varchar(128),
  `sat_key` varchar(128),
  `sat_description` varchar(128),
  `sat_link` varchar(128),
  `source_concat` varchar(256),
  `ind_current` int(11),
  `source_hub_1_business_key` varchar(128),
  `source_hub_2_business_key` varchar(128),
  `source_hub_3_business_key` varchar(128),
  `source_hub_4_business_key` varchar(128),
  `source_hub_5_business_key` varchar(128),
  `source_hub_6_business_key` varchar(128),
  `source_hub_7_business_key` varchar(128),
  `source_hub_8_business_key` varchar(128),
  `source_hub_9_business_key` varchar(128),
  `source_hub_10_business_key` varchar(128),
  `source_lnk_key_attribute_1` varchar(128),
  `source_lnk_key_attribute_2` varchar(128),
  `source_lnk_key_attribute_3` varchar(128),
  `source_lnk_key_attribute_4` varchar(128),
  `source_lnk_key_attribute_5` varchar(128),
  `attribute_number` int(11),
  `attribute_source_column` varchar(128),
  `attribute_target_column` varchar(128)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `stg_management_links`
--

DROP TABLE IF EXISTS `stg_management_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stg_management_links` (
  `link_name` varchar(128) DEFAULT NULL,
  `link_key` varchar(128) DEFAULT NULL,
  `link_description` varchar(128) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `source_concat` varchar(256) DEFAULT NULL,
  `hub_1` varchar(128) DEFAULT NULL,
  `link_hub_1_key_column` varchar(128) DEFAULT NULL,
  `source_hub_1_business_key` varchar(128) DEFAULT NULL,
  `hub_2` varchar(128) DEFAULT NULL,
  `link_hub_2_key_column` varchar(128) DEFAULT NULL,
  `source_hub_2_business_key` varchar(128) DEFAULT NULL,
  `hub_3` varchar(128) DEFAULT NULL,
  `link_hub_3_key_column` varchar(128) DEFAULT NULL,
  `source_hub_3_business_key` varchar(128) DEFAULT NULL,
  `hub_4` varchar(128) DEFAULT NULL,
  `link_hub_4_key_column` varchar(128) DEFAULT NULL,
  `source_hub_4_business_key` varchar(128) DEFAULT NULL,
  `hub_5` varchar(128) DEFAULT NULL,
  `link_hub_5_key_column` varchar(128) DEFAULT NULL,
  `source_hub_5_business_key` varchar(128) DEFAULT NULL,
  `hub_6` varchar(128) DEFAULT NULL,
  `link_hub_6_key_column` varchar(128) DEFAULT NULL,
  `source_hub_6_business_key` varchar(128) DEFAULT NULL,
  `hub_7` varchar(128) DEFAULT NULL,
  `link_hub_7_key_column` varchar(128) DEFAULT NULL,
  `source_hub_7_business_key` varchar(128) DEFAULT NULL,
  `hub_8` varchar(128) DEFAULT NULL,
  `link_hub_8_key_column` varchar(128) DEFAULT NULL,
  `source_hub_8_business_key` varchar(128) DEFAULT NULL,
  `hub_9` varchar(128) DEFAULT NULL,
  `link_hub_9_key_column` varchar(128) DEFAULT NULL,
  `source_hub_9_business_key` varchar(128) DEFAULT NULL,
  `hub_10` varchar(128) DEFAULT NULL,
  `link_hub_10_key_column` varchar(128) DEFAULT NULL,
  `source_hub_10_business_key` varchar(128) DEFAULT NULL,
  `link_source_order` int(11) DEFAULT NULL,
  `lnk_bk_columns` varchar(256) DEFAULT NULL,
  `lnk_no_bk_columns` varchar(256) DEFAULT NULL,
  `ind_last_seen_dts` int(11) DEFAULT NULL,
  `ind_status_sat` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_runtypes_hist`
--

DROP TABLE IF EXISTS `ref_runtypes_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_runtypes_hist` (
  `id_rtyp` tinyint(4) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `dml_operation` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_rtyp`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_data_vault_link_attributes_hist`
--

DROP TABLE IF EXISTS `ref_data_vault_link_attributes_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_link_attributes_hist` (
  `id_data_vault_link_attribute` int(11) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `id_data_vault_link` int(11) DEFAULT NULL,
  `record_source_id` int(11) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `link_attributes` varchar(8092) DEFAULT NULL,
  `link_attributes_concat` varchar(8092) DEFAULT NULL,
  `link_attributes_concat_dv` varchar(8092) DEFAULT NULL,
  `link_attributes_dv` varchar(8092) DEFAULT NULL,
  `dml_operation` char(1) NOT NULL,
  PRIMARY KEY (`id_data_vault_link_attribute`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inst_run_transformations`
--

DROP TABLE IF EXISTS `inst_run_transformations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inst_run_transformations` (
  `id_run` int(11) DEFAULT NULL,
  `transformation` varchar(512) DEFAULT NULL,
  `etl_id_job` int(11) DEFAULT NULL,
  `etl_id_batch` int(11) DEFAULT NULL,
  `etl_date_insert` datetime DEFAULT NULL,
  `record_source_id` int(11) DEFAULT NULL,
  `data_vault_hub` varchar(128) DEFAULT NULL,
  `data_vault_hub_sat` varchar(128) DEFAULT NULL,
  `data_vault_link` varchar(128) DEFAULT NULL,
  `data_vault_link_sat` varchar(128) DEFAULT NULL,
  KEY `fk_inst_run_transformations_runs` (`id_run`) USING BTREE,
  KEY `fk_inst_run_transformations_transformations` (`transformation`) USING BTREE,
  CONSTRAINT `fk_inst_run_transformations_runs` FOREIGN KEY (`id_run`) REFERENCES `inst_runs` (`id_run`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_transformations`
--

DROP TABLE IF EXISTS `ref_transformations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_transformations` (
  `transformation` varchar(512) NOT NULL,
  `id_prcstg` tinyint(4) DEFAULT NULL,
  `id_srcsys` tinyint(4) DEFAULT NULL,
  `ind_current` tinyint(4) NOT NULL,
  `processing_order` int(11) DEFAULT NULL,
  `hist_date_insert` datetime NOT NULL,
  `dml_operation` char(1) DEFAULT NULL,
  PRIMARY KEY (`transformation`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_data_vault_hub_sources`
--

DROP TABLE IF EXISTS `ref_data_vault_hub_sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_hub_sources` (
  `id_data_vault_hub` int(11) NOT NULL,
  `record_source_id` int(11) NOT NULL,
  `source_business_key` varchar(128) NOT NULL DEFAULT '',
  `source_order` int(11) NOT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `ind_status_sat` int(11) DEFAULT '0',
  PRIMARY KEY (`id_data_vault_hub`,`record_source_id`,`source_business_key`) USING BTREE,
  KEY `fk_ref_dv_hub_sources_source_tables` (`record_source_id`),
  CONSTRAINT `fk_ref_dv_hub_sources_hubs` FOREIGN KEY (`id_data_vault_hub`) REFERENCES `ref_data_vault_hubs` (`id_data_vault_hub`),
  CONSTRAINT `fk_ref_dv_hub_sources_source_tables` FOREIGN KEY (`record_source_id`) REFERENCES `ref_source_tables` (`id_srctab`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_ref_dv_hub_sources_after_i` AFTER INSERT ON `ref_data_vault_hub_sources`
FOR EACH ROW
INSERT 
INTO 
    ref_data_vault_hub_sources_hist 
    (
      id_data_vault_hub
     ,record_source_id
     ,hist_date_insert 
     ,source_business_key
     ,source_order
     ,ind_current
     ,ind_status_sat
     ,dml_operation
    ) 
    VALUES 
    (
        NEW.id_data_vault_hub,
        NEW.record_source_id,
        NOW(),
        NEW.source_business_key,
        NEW.source_order,
        NEW.ind_current,
        NEW.ind_status_sat,
        'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_ref_dv_hub_sources_after_u` AFTER UPDATE ON `ref_data_vault_hub_sources`
FOR EACH ROW
IF     ifnull(NEW.source_order,-1)     <> ifnull(OLD.source_order,-1)
or     ifnull(NEW.ind_current,-1)      <> ifnull(OLD.ind_current,-1)
or     ifnull(NEW.ind_status_sat,-1)   <> ifnull(OLD.ind_status_sat,-1)
THEN
INSERT INTO 
    ref_data_vault_hub_sources_hist 
    (
      id_data_vault_hub
     ,record_source_id
     ,hist_date_insert 
     ,source_business_key
     ,source_order
     ,ind_current
     ,ind_status_sat
     ,dml_operation
    ) 
    VALUES 
    (
        NEW.id_data_vault_hub,
        NEW.record_source_id,
        NOW(),
        NEW.source_business_key,
        NEW.source_order,
        NEW.ind_current,
        NEW.ind_status_sat,
        'U'
    );
END IF */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_ref_dv_hub_sources_after_d` AFTER DELETE ON `ref_data_vault_hub_sources`
FOR EACH ROW
INSERT
INTO 
    ref_data_vault_hub_sources_hist 
    (
      id_data_vault_hub
     ,record_source_id
     ,hist_date_insert 
     ,source_business_key
     ,source_order
     ,ind_current
     ,ind_status_sat
     ,dml_operation
    ) 
    VALUES 
    (
        OLD.id_data_vault_hub,
        OLD.record_source_id,
        NOW(),
        OLD.source_business_key,
        OLD.source_order,
        OLD.ind_current,
        OLD.ind_status_sat,
        'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary table structure for view `vw_inst_transformation_parameters_pivot_run`
--

DROP TABLE IF EXISTS `vw_inst_transformation_parameters_pivot_run`;
/*!50001 DROP VIEW IF EXISTS `vw_inst_transformation_parameters_pivot_run`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_inst_transformation_parameters_pivot_run` (
  `transformation` varchar(512),
  `repodir_dimensional` varchar(128),
  `id_run` int(11),
  `hist_datum` datetime
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ref_data_vault_link_sources_hist`
--

DROP TABLE IF EXISTS `ref_data_vault_link_sources_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_link_sources_hist` (
  `id_data_vault_link` int(11) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `source_hub_1_business_key` varchar(128) DEFAULT NULL,
  `source_hub_2_business_key` varchar(128) DEFAULT NULL,
  `source_hub_3_business_key` varchar(128) DEFAULT NULL,
  `source_hub_4_business_key` varchar(128) DEFAULT NULL,
  `source_hub_5_business_key` varchar(128) DEFAULT NULL,
  `source_hub_6_business_key` varchar(128) DEFAULT NULL,
  `source_hub_7_business_key` varchar(128) DEFAULT NULL,
  `source_hub_8_business_key` varchar(128) DEFAULT NULL,
  `source_hub_9_business_key` varchar(128) DEFAULT NULL,
  `source_hub_10_business_key` varchar(128) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `record_source_id` int(11) NOT NULL DEFAULT '0',
  `source_order` int(11) NOT NULL DEFAULT '0',
  `dml_operation` char(1) NOT NULL,
  `ind_status_sat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_data_vault_link`,`record_source_id`,`source_order`,`hist_date_insert`) USING BTREE,
  KEY `fk_ref_dv_link_sources_hist_source_tables` (`record_source_id`),
  CONSTRAINT `fk_ref_dv_link_sources_hist_source_links` FOREIGN KEY (`id_data_vault_link`) REFERENCES `ref_data_vault_links` (`id_data_vault_link`),
  CONSTRAINT `fk_ref_dv_link_sources_hist_source_tables` FOREIGN KEY (`record_source_id`) REFERENCES `ref_source_tables` (`id_srctab`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_transformation_parameters_hist`
--

DROP TABLE IF EXISTS `ref_transformation_parameters_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_transformation_parameters_hist` (
  `transformation` varchar(512) NOT NULL,
  `parameter_name` varchar(128) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `parameter_value_varchar` varchar(8000) DEFAULT NULL,
  `parameter_value_datetime` datetime DEFAULT NULL,
  `parameter_value_numeric` decimal(12,2) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `dml_operation` char(1) DEFAULT NULL,
  PRIMARY KEY (`transformation`,`parameter_name`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inst_run_dv_jobs`
--

DROP TABLE IF EXISTS `inst_run_dv_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inst_run_dv_jobs` (
  `id_run` int(11) NOT NULL,
  `job` varchar(512) NOT NULL,
  `transformation` varchar(512) NOT NULL,
  `data_vault_object` varchar(128) NOT NULL,
  `record_source_id` int(11) NOT NULL,
  `source_order` int(11) NOT NULL DEFAULT '1',
  `data_vault_hub` varchar(128) DEFAULT NULL,
  `data_vault_hub_sat` varchar(128) DEFAULT NULL,
  `data_vault_link` varchar(128) DEFAULT NULL,
  `data_vault_link_sat` varchar(128) DEFAULT NULL,
  `date_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` datetime DEFAULT NULL,
  `num_records_start` int(11) DEFAULT NULL,
  `num_records_end` int(11) DEFAULT NULL,
  `num_errors` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_run`,`job`,`transformation`,`data_vault_object`,`record_source_id`,`source_order`,`date_start`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_management_link_attributes`
--

DROP TABLE IF EXISTS `stg_management_link_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stg_management_link_attributes` (
  `link_name` varchar(128) DEFAULT NULL,
  `source_concat` varchar(256) DEFAULT NULL,
  `attribute_number` int(11) DEFAULT NULL,
  `attribute_source_column` varchar(128) DEFAULT NULL,
  `attribute_target_column` varchar(128) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `sheet_row_number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_data_vault_hub_satellites_hist`
--

DROP TABLE IF EXISTS `ref_data_vault_hub_satellites_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_hub_satellites_hist` (
  `id_data_vault_hub_sat` int(11) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `id_data_vault_hub` int(11) DEFAULT NULL,
  `sat_name` varchar(128) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `sat_source_hub_business_key` varchar(128) DEFAULT NULL,
  `sat_attributes` varchar(8092) DEFAULT NULL,
  `sat_attributes_concat` varchar(8092) DEFAULT NULL,
  `sat_attributes_concat_dv` varchar(8092) DEFAULT NULL,
  `record_source_id` int(11) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `sat_attributes_dv` varchar(8092) DEFAULT NULL,
  `sat_key` varchar(128) DEFAULT NULL,
  `dml_operation` char(1) NOT NULL,
  `ind_multiactive_extra_key_column` int(11) DEFAULT '0',
  `sat_multiactive_extra_key_column` varchar(128) DEFAULT NULL,
  `sat_multiactive_extra_key_column_src` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_data_vault_hub_sat`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_data_vault_link_sources`
--

DROP TABLE IF EXISTS `ref_data_vault_link_sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_link_sources` (
  `id_data_vault_link` int(11) NOT NULL,
  `source_hub_1_business_key` varchar(128) DEFAULT NULL,
  `source_hub_2_business_key` varchar(128) DEFAULT NULL,
  `source_hub_3_business_key` varchar(128) DEFAULT NULL,
  `source_hub_4_business_key` varchar(128) DEFAULT NULL,
  `source_hub_5_business_key` varchar(128) DEFAULT NULL,
  `source_hub_6_business_key` varchar(128) DEFAULT NULL,
  `source_hub_7_business_key` varchar(128) DEFAULT NULL,
  `source_hub_8_business_key` varchar(128) DEFAULT NULL,
  `source_hub_9_business_key` varchar(128) DEFAULT NULL,
  `source_hub_10_business_key` varchar(128) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `record_source_id` int(11) NOT NULL DEFAULT '0',
  `source_order` int(11) DEFAULT NULL,
  `ind_status_sat` int(11) DEFAULT '0',
  PRIMARY KEY (`id_data_vault_link`,`record_source_id`) USING BTREE,
  KEY `fk_ref_dv_link_sources_source_tables` (`record_source_id`),
  CONSTRAINT `fk_ref_dv_link_sources_source_links` FOREIGN KEY (`id_data_vault_link`) REFERENCES `ref_data_vault_links` (`id_data_vault_link`),
  CONSTRAINT `fk_ref_dv_link_sources_source_tables` FOREIGN KEY (`record_source_id`) REFERENCES `ref_source_tables` (`id_srctab`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_ref_dv_link_sources_after_i` AFTER INSERT ON `ref_data_vault_link_sources`
FOR EACH ROW
INSERT 
INTO 
    ref_data_vault_link_sources_hist 
    (   
      id_data_vault_link
     ,hist_date_insert      
     ,source_hub_1_business_key  
     ,source_hub_2_business_key      
     ,source_hub_3_business_key       
     ,source_hub_4_business_key  
     ,source_hub_5_business_key     
     ,source_hub_6_business_key    
     ,source_hub_7_business_key    
     ,source_hub_8_business_key     
     ,source_hub_9_business_key  
     ,source_hub_10_business_key
     ,record_source_id
     ,source_order
     ,ind_current
     ,ind_status_sat
     ,dml_operation
    ) 
    VALUES 
    (
      NEW.id_data_vault_link
     ,NOW()
     ,NEW.source_hub_1_business_key       
     ,NEW.source_hub_2_business_key  
     ,NEW.source_hub_3_business_key 
     ,NEW.source_hub_4_business_key    
     ,NEW.source_hub_5_business_key    
     ,NEW.source_hub_6_business_key   
     ,NEW.source_hub_7_business_key    
     ,NEW.source_hub_8_business_key    
     ,NEW.source_hub_9_business_key  
     ,NEW.source_hub_10_business_key
     ,NEW.record_source_id
     ,NEW.source_order
     ,NEW.ind_current
     ,NEW.ind_status_sat
     ,'I' 
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_ref_dv_link_sources_after_u` AFTER UPDATE ON `ref_data_vault_link_sources`
FOR EACH ROW
IF     ifnull(NEW.source_hub_1_business_key,'')  <> ifnull(OLD.source_hub_1_business_key,'')
or     ifnull(NEW.source_hub_2_business_key,'')  <> ifnull(OLD.source_hub_2_business_key,'')
or     ifnull(NEW.source_hub_3_business_key,'')  <> ifnull(OLD.source_hub_3_business_key,'')
or     ifnull(NEW.source_hub_4_business_key,'')  <> ifnull(OLD.source_hub_4_business_key,'')
or     ifnull(NEW.source_hub_5_business_key,'')  <> ifnull(OLD.source_hub_5_business_key,'')
or     ifnull(NEW.source_hub_6_business_key,'')  <> ifnull(OLD.source_hub_6_business_key,'')
or     ifnull(NEW.source_hub_7_business_key,'')  <> ifnull(OLD.source_hub_7_business_key,'')
or     ifnull(NEW.source_hub_8_business_key,'')  <> ifnull(OLD.source_hub_8_business_key,'')
or     ifnull(NEW.source_hub_9_business_key,'')  <> ifnull(OLD.source_hub_9_business_key,'')
or     ifnull(NEW.source_hub_10_business_key,'') <> ifnull(OLD.source_hub_10_business_key,'')
or     ifnull(NEW.ind_current,-1)                <> ifnull(OLD.ind_current,-1)
or     ifnull(NEW.record_source_id,-1)           <> ifnull(OLD.record_source_id,-1)
or     ifnull(NEW.source_order,-1)               <> ifnull(OLD.source_order,-1)
or     ifnull(NEW.ind_status_sat,-1)             <> ifnull(OLD.ind_status_sat,-1)
THEN
INSERT 
INTO 
    ref_data_vault_link_sources_hist 
    (   
      id_data_vault_link
     ,hist_date_insert     
     ,source_hub_1_business_key     
     ,source_hub_2_business_key   
     ,source_hub_3_business_key    
     ,source_hub_4_business_key    
     ,source_hub_5_business_key   
     ,source_hub_6_business_key      
     ,source_hub_7_business_key      
     ,source_hub_8_business_key    
     ,source_hub_9_business_key  
     ,source_hub_10_business_key
     ,record_source_id
     ,source_order
     ,ind_current
     ,ind_status_sat
     ,dml_operation
    ) 
    VALUES 
    (
      NEW.id_data_vault_link
     ,NOW()    
     ,NEW.source_hub_1_business_key       
     ,NEW.source_hub_2_business_key      
     ,NEW.source_hub_3_business_key       
     ,NEW.source_hub_4_business_key     
     ,NEW.source_hub_5_business_key   
     ,NEW.source_hub_6_business_key     
     ,NEW.source_hub_7_business_key 
     ,NEW.source_hub_8_business_key   
     ,NEW.source_hub_9_business_key   
     ,NEW.source_hub_10_business_key
     ,NEW.record_source_id
     ,NEW.source_order
     ,NEW.ind_current
     ,NEW.ind_status_sat
     ,'U' 
    );
END IF */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_ref_dv_link_sources_after_d` AFTER DELETE ON `ref_data_vault_link_sources`
FOR EACH ROW
INSERT 
INTO 
    ref_data_vault_link_sources_hist 
    (   
      id_data_vault_link
     ,hist_date_insert   
     ,source_hub_1_business_key  
     ,source_hub_2_business_key     
     ,source_hub_3_business_key     
     ,source_hub_4_business_key
     ,source_hub_5_business_key    
     ,source_hub_6_business_key    
     ,source_hub_7_business_key    
     ,source_hub_8_business_key 
     ,source_hub_9_business_key   
     ,source_hub_10_business_key
     ,record_source_id
     ,source_order
     ,ind_current
     ,ind_status_sat
     ,dml_operation
    ) 
    VALUES 
    (
      OLD.id_data_vault_link
     ,NOW()      
     ,OLD.source_hub_1_business_key 
     ,OLD.source_hub_2_business_key       
     ,OLD.source_hub_3_business_key      
     ,OLD.source_hub_4_business_key   
     ,OLD.source_hub_5_business_key   
     ,OLD.source_hub_6_business_key    
     ,OLD.source_hub_7_business_key    
     ,OLD.source_hub_8_business_key      
     ,OLD.source_hub_9_business_key  
     ,OLD.source_hub_10_business_key
     ,OLD.record_source_id
     ,OLD.source_order
     ,OLD.ind_current
     ,OLD.ind_status_sat
     ,'D' 
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ref_data_vaults`
--

DROP TABLE IF EXISTS `ref_data_vaults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vaults` (
  `id_data_vault` int(11) NOT NULL,
  `data_vault_name` varchar(128) NOT NULL,
  `data_vault_description` varchar(512) NOT NULL,
  `ind_current` int(11) NOT NULL,
  PRIMARY KEY (`id_data_vault`) USING BTREE,
  UNIQUE KEY `name` (`data_vault_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_data_vault_after_i AFTER INSERT ON ref_data_vaults FOR EACH ROW INSERT 
INTO 
    ref_data_vaults_hist 
    (
      id_data_vault
     ,hist_date_insert 
     ,data_vault_name
     ,data_vault_description               
     ,ind_current
     ,dml_operation
    ) 
    VALUES 
    (
      NEW.id_data_vault
     ,NOW()
     ,NEW.data_vault_name               
     ,NEW.data_vault_description    
     ,NEW.ind_current
     , 'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_data_vault_after_u AFTER UPDATE ON ref_data_vaults FOR EACH ROW INSERT 
INTO 
    ref_data_vaults_hist 
    (
      id_data_vault
     ,hist_date_insert 
     ,data_vault_name
     ,data_vault_description             
     ,ind_current
     ,dml_operation
    ) 
    VALUES 
    (
      NEW.id_data_vault
     ,NOW()
     ,NEW.data_vault_name               
     ,NEW.data_vault_description    
     ,NEW.ind_current
     , 'U'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_data_vault_after_d AFTER DELETE ON ref_data_vaults FOR EACH ROW INSERT 
INTO 
    ref_data_vaults_hist 
    (
      id_data_vault
     ,hist_date_insert 
     ,data_vault_name
     ,data_vault_description           
     ,ind_current
     ,dml_operation
    ) 
    VALUES 
    (
      OLD.id_data_vault
     ,NOW()
     ,OLD.data_vault_name               
     ,OLD.data_vault_description    
     ,OLD.ind_current
     , 'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ref_source_systems`
--

DROP TABLE IF EXISTS `ref_source_systems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_source_systems` (
  `id_srcsys` tinyint(4) NOT NULL,
  `cod_srcsys` varchar(16) NOT NULL,
  `description` varchar(128) NOT NULL,
  `id_source_connection` int(11) DEFAULT NULL,
  `id_staging_connection` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_srcsys`) USING BTREE,
  KEY `fk_ref_source_system_staging_connection` (`id_staging_connection`),
  KEY `fk_ref_source_system_source_connection` (`id_source_connection`),
  CONSTRAINT `fk_ref_source_system_source_connection` FOREIGN KEY (`id_source_connection`) REFERENCES `ref_connections` (`id_connection`),
  CONSTRAINT `fk_ref_source_system_staging_connection` FOREIGN KEY (`id_staging_connection`) REFERENCES `ref_connections` (`id_connection`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_srcsys_after_i AFTER INSERT ON ref_source_systems FOR EACH ROW INSERT 
INTO 
    ref_source_systems_hist 
    (
        id_srcsys ,
        hist_date_insert ,
        cod_srcsys ,
        description ,
        id_source_connection,
        id_staging_connection,
        dml_operation
    ) 
    VALUES 
    (
        NEW.id_srcsys ,
        NOW() ,
        NEW.cod_srcsys ,
        NEW.description ,
        NEW.id_source_connection,
        NEW.id_staging_connection,
        'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_srcsys_after_u AFTER UPDATE ON ref_source_systems FOR EACH ROW INSERT 
INTO 
    ref_source_systems_hist 
    (
        id_srcsys ,
        hist_date_insert ,
        cod_srcsys ,
        description ,
        id_source_connection,
        id_staging_connection,
        dml_operation
    ) 
    VALUES 
    (
        NEW.id_srcsys ,
        NOW() ,
        NEW.cod_srcsys ,
        NEW.description ,
        NEW.id_source_connection,
        NEW.id_staging_connection,
        'U'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_srcsys_after_d AFTER DELETE ON ref_source_systems FOR EACH ROW INSERT 
INTO 
    ref_source_systems_hist 
    (
        id_srcsys ,
        hist_date_insert ,
        cod_srcsys ,
        description ,
        id_source_connection,
        id_staging_connection,
        dml_operation
    ) 
    VALUES 
    (
        OLD.id_srcsys ,
        NOW() ,
        OLD.cod_srcsys ,
        OLD.description ,
        OLD.id_source_connection,
        OLD.id_staging_connection,
        'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ref_data_vaults_hist`
--

DROP TABLE IF EXISTS `ref_data_vaults_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vaults_hist` (
  `id_data_vault` int(11) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `data_vault_name` varchar(128) NOT NULL,
  `data_vault_description` varchar(512) NOT NULL,
  `ind_current` int(11) NOT NULL,
  `dml_operation` char(1) NOT NULL,
  PRIMARY KEY (`id_data_vault`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `etl_log_transformation_step_performance`
--

DROP TABLE IF EXISTS `etl_log_transformation_step_performance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etl_log_transformation_step_performance` (
  `ID_BATCH` int(11) DEFAULT NULL,
  `SEQ_NR` int(11) DEFAULT NULL,
  `LOGDATE` datetime DEFAULT NULL,
  `TRANSNAME` varchar(255) DEFAULT NULL,
  `STEPNAME` varchar(255) DEFAULT NULL,
  `STEP_COPY` int(11) DEFAULT NULL,
  `LINES_READ` bigint(20) DEFAULT NULL,
  `LINES_WRITTEN` bigint(20) DEFAULT NULL,
  `LINES_UPDATED` bigint(20) DEFAULT NULL,
  `LINES_INPUT` bigint(20) DEFAULT NULL,
  `LINES_OUTPUT` bigint(20) DEFAULT NULL,
  `LINES_REJECTED` bigint(20) DEFAULT NULL,
  `ERRORS` bigint(20) DEFAULT NULL,
  `INPUT_BUFFER_ROWS` bigint(20) DEFAULT NULL,
  `OUTPUT_BUFFER_ROWS` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inst_runs`
--

DROP TABLE IF EXISTS `inst_runs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inst_runs` (
  `id_run` int(11) NOT NULL,
  `id_rtyp` tinyint(4) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `id_status` tinyint(4) DEFAULT NULL,
  `error_message` varchar(256) DEFAULT NULL,
  `ind_restart` int(11) DEFAULT '0',
  `load_dts` datetime DEFAULT NULL,
  PRIMARY KEY (`id_run`) USING BTREE,
  KEY `id_rtyp` (`id_rtyp`) USING BTREE,
  KEY `id_status` (`id_status`) USING BTREE,
  CONSTRAINT `inst_runs_ibfk_1` FOREIGN KEY (`id_rtyp`) REFERENCES `ref_runtypes` (`id_rtyp`),
  CONSTRAINT `inst_runs_ibfk_2` FOREIGN KEY (`id_status`) REFERENCES `ref_statuses` (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_system_parameters`
--

DROP TABLE IF EXISTS `ref_system_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_system_parameters` (
  `parameter_name` varchar(128) NOT NULL,
  `parameter_value_varchar` varchar(128) DEFAULT NULL,
  `parameter_value_datetime` datetime DEFAULT NULL,
  `parameter_value_numeric` decimal(12,2) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`parameter_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_syspar_after_i AFTER INSERT ON ref_system_parameters FOR EACH ROW INSERT 
INTO 
    ref_system_parameters_hist 
    (
        parameter_name ,
        hist_date_insert ,
        parameter_value_varchar ,
        parameter_value_datetime ,
        parameter_value_numeric ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        NEW.parameter_name ,
        NOW() ,
        NEW.parameter_value_varchar ,
        NEW.parameter_value_datetime ,
        NEW.parameter_value_numeric ,
        NEW.description ,
        'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_syspar_after_u AFTER UPDATE ON ref_system_parameters FOR EACH ROW INSERT 
INTO 
    ref_system_parameters_hist 
    (
        parameter_name ,
        hist_date_insert ,
        parameter_value_varchar ,
        parameter_value_datetime ,
        parameter_value_numeric ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        NEW.parameter_name ,
        NOW() ,
        NEW.parameter_value_varchar ,
        NEW.parameter_value_datetime ,
        NEW.parameter_value_numeric ,
        NEW.description ,
        'U'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_syspar_after_d AFTER DELETE ON ref_system_parameters FOR EACH ROW INSERT 
INTO 
    ref_system_parameters_hist 
    (
        parameter_name ,
        hist_date_insert ,
        parameter_value_varchar ,
        parameter_value_datetime ,
        parameter_value_numeric ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        OLD.parameter_name ,
        NOW() ,
        OLD.parameter_value_varchar ,
        OLD.parameter_value_datetime ,
        OLD.parameter_value_numeric ,
        OLD.description ,
        'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ref_transformations_hist`
--

DROP TABLE IF EXISTS `ref_transformations_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_transformations_hist` (
  `transformation` varchar(512) NOT NULL,
  `id_prcstg` tinyint(4) DEFAULT NULL,
  `id_srcsys` tinyint(4) DEFAULT NULL,
  `ind_current` tinyint(4) NOT NULL,
  `processing_order` int(11) DEFAULT NULL,
  `hist_date_insert` datetime NOT NULL,
  `dml_operation` char(1) DEFAULT NULL,
  PRIMARY KEY (`transformation`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `etl_log_transformation_step`
--

DROP TABLE IF EXISTS `etl_log_transformation_step`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etl_log_transformation_step` (
  `ID_BATCH` int(11) DEFAULT NULL,
  `CHANNEL_ID` varchar(255) DEFAULT NULL,
  `LOG_DATE` datetime DEFAULT NULL,
  `TRANSNAME` varchar(255) DEFAULT NULL,
  `STEPNAME` varchar(255) DEFAULT NULL,
  `STEP_COPY` int(11) DEFAULT NULL,
  `LINES_READ` bigint(20) DEFAULT NULL,
  `LINES_WRITTEN` bigint(20) DEFAULT NULL,
  `LINES_UPDATED` bigint(20) DEFAULT NULL,
  `LINES_INPUT` bigint(20) DEFAULT NULL,
  `LINES_OUTPUT` bigint(20) DEFAULT NULL,
  `LINES_REJECTED` bigint(20) DEFAULT NULL,
  `ERRORS` bigint(20) DEFAULT NULL,
  `LOG_FIELD` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inst_actual_runs`
--

DROP TABLE IF EXISTS `inst_actual_runs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inst_actual_runs` (
  `id_run` int(11) NOT NULL AUTO_INCREMENT,
  `id_rtyp` tinyint(4) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `id_status` tinyint(4) DEFAULT NULL,
  `error_message` varchar(256) DEFAULT NULL,
  `ind_restart` int(11) DEFAULT '0',
  `load_dts` datetime DEFAULT NULL,
  PRIMARY KEY (`id_run`) USING BTREE,
  KEY `id_rtyp` (`id_rtyp`) USING BTREE,
  KEY `id_status` (`id_status`) USING BTREE,
  CONSTRAINT `inst_actual_runs_ibfk_1` FOREIGN KEY (`id_rtyp`) REFERENCES `ref_runtypes` (`id_rtyp`),
  CONSTRAINT `inst_actual_runs_ibfk_2` FOREIGN KEY (`id_status`) REFERENCES `ref_statuses` (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_inst_arun_after_i` AFTER INSERT ON `inst_actual_runs`
FOR EACH ROW
INSERT INTO inst_runs 
(    id_run    ,id_rtyp    ,date_start    ,date_end    ,id_status    ,error_message    ,ind_restart,    load_dts) VALUES
(NEW.id_run,NEW.id_rtyp,NEW.date_start,NEW.date_end,NEW.id_status,NEW.error_message,NEW.ind_restart,NEW.load_dts) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_inst_arun_after_u` AFTER UPDATE ON `inst_actual_runs`
FOR EACH ROW
UPDATE inst_runs SET id_rtyp = NEW.id_rtyp ,
date_start = NEW.date_start ,
date_end = NEW.date_end ,
id_status = NEW.id_status ,
error_message = NEW.error_message,
ind_restart = NEW.ind_restart,
load_dts = NEW.load_dts
WHERE id_run = NEW.id_run */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary table structure for view `vw_ref_transformation_parameters_pivot`
--

DROP TABLE IF EXISTS `vw_ref_transformation_parameters_pivot`;
/*!50001 DROP VIEW IF EXISTS `vw_ref_transformation_parameters_pivot`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_ref_transformation_parameters_pivot` (
  `transformation` varchar(512)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `etl_log_transformation`
--

DROP TABLE IF EXISTS `etl_log_transformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etl_log_transformation` (
  `ID_BATCH` int(11) DEFAULT NULL,
  `CHANNEL_ID` varchar(255) DEFAULT NULL,
  `TRANSNAME` varchar(255) DEFAULT NULL,
  `STATUS` varchar(15) DEFAULT NULL,
  `LINES_READ` bigint(20) DEFAULT NULL,
  `LINES_WRITTEN` bigint(20) DEFAULT NULL,
  `LINES_UPDATED` bigint(20) DEFAULT NULL,
  `LINES_INPUT` bigint(20) DEFAULT NULL,
  `LINES_OUTPUT` bigint(20) DEFAULT NULL,
  `LINES_REJECTED` bigint(20) DEFAULT NULL,
  `ERRORS` bigint(20) DEFAULT NULL,
  `STARTDATE` datetime DEFAULT NULL,
  `ENDDATE` datetime DEFAULT NULL,
  `LOGDATE` datetime DEFAULT NULL,
  `DEPDATE` datetime DEFAULT NULL,
  `REPLAYDATE` datetime DEFAULT NULL,
  `LOG_FIELD` mediumtext,
  KEY `IDX_etl_log_transformation_1` (`ID_BATCH`),
  KEY `IDX_etl_log_transformation_2` (`ERRORS`,`STATUS`,`TRANSNAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `etl_log_job`
--

DROP TABLE IF EXISTS `etl_log_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etl_log_job` (
  `ID_JOB` int(11) DEFAULT NULL,
  `CHANNEL_ID` varchar(255) DEFAULT NULL,
  `JOBNAME` varchar(255) DEFAULT NULL,
  `STATUS` varchar(15) DEFAULT NULL,
  `LINES_READ` bigint(20) DEFAULT NULL,
  `LINES_WRITTEN` bigint(20) DEFAULT NULL,
  `LINES_UPDATED` bigint(20) DEFAULT NULL,
  `LINES_INPUT` bigint(20) DEFAULT NULL,
  `LINES_OUTPUT` bigint(20) DEFAULT NULL,
  `LINES_REJECTED` bigint(20) DEFAULT NULL,
  `ERRORS` bigint(20) DEFAULT NULL,
  `STARTDATE` datetime DEFAULT NULL,
  `ENDDATE` datetime DEFAULT NULL,
  `LOGDATE` datetime DEFAULT NULL,
  `DEPDATE` datetime DEFAULT NULL,
  `REPLAYDATE` datetime DEFAULT NULL,
  `LOG_FIELD` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_data_vault_hub_sources_hist`
--

DROP TABLE IF EXISTS `ref_data_vault_hub_sources_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_hub_sources_hist` (
  `id_data_vault_hub` int(11) NOT NULL,
  `record_source_id` int(11) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `source_business_key` varchar(128) NOT NULL DEFAULT '',
  `source_order` int(11) NOT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `dml_operation` char(1) NOT NULL,
  `ind_status_sat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_data_vault_hub`,`record_source_id`,`source_business_key`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `vw_stg_management_satellites_all_columns_filled`
--

DROP TABLE IF EXISTS `vw_stg_management_satellites_all_columns_filled`;
/*!50001 DROP VIEW IF EXISTS `vw_stg_management_satellites_all_columns_filled`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_stg_management_satellites_all_columns_filled` (
  `sat_name` varchar(128),
  `sat_key` varchar(128),
  `sat_description` varchar(128),
  `sat_hub` varchar(128),
  `source_concat` varchar(256),
  `ind_current` int(11),
  `source_hub_business_key` varchar(128),
  `attribute_number` int(11),
  `attribute_source_column` varchar(128),
  `attribute_target_column` varchar(128),
  `ind_multiactive_extra_key_column` int(11)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_ref_system_parameters_pivot`
--

DROP TABLE IF EXISTS `vw_ref_system_parameters_pivot`;
/*!50001 DROP VIEW IF EXISTS `vw_ref_system_parameters_pivot`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_ref_system_parameters_pivot` (
  `repodir_dimensional` varchar(128)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `etl_log_counter`
--

DROP TABLE IF EXISTS `etl_log_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etl_log_counter` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5103 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_transformation_parameters`
--

DROP TABLE IF EXISTS `ref_transformation_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_transformation_parameters` (
  `transformation` varchar(512) NOT NULL,
  `parameter_name` varchar(128) NOT NULL,
  `parameter_value_varchar` varchar(8000) DEFAULT NULL,
  `parameter_value_datetime` datetime DEFAULT NULL,
  `parameter_value_numeric` decimal(12,2) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`transformation`,`parameter_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_trfpar_after_i AFTER INSERT ON ref_transformation_parameters FOR EACH ROW INSERT 
INTO 
    ref_transformation_parameters_hist 
    (
        transformation ,
        parameter_name ,
        hist_date_insert ,
        parameter_value_varchar ,
        parameter_value_datetime ,
        parameter_value_numeric ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        NEW.transformation ,
        NEW.parameter_name ,
        NOW() ,
        NEW.parameter_value_varchar ,
        NEW.parameter_value_datetime ,
        NEW.parameter_value_numeric ,
        NEW.description ,
        'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_trfpar_after_u AFTER UPDATE ON ref_transformation_parameters FOR EACH ROW INSERT 
INTO 
    ref_transformation_parameters_hist 
    (
        transformation ,
        parameter_name ,
        hist_date_insert ,
        parameter_value_varchar ,
        parameter_value_datetime ,
        parameter_value_numeric ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        NEW.transformation ,
        NEW.parameter_name ,
        NOW() ,
        NEW.parameter_value_varchar ,
        NEW.parameter_value_datetime ,
        NEW.parameter_value_numeric ,
        NEW.description ,
        'U'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_trfpar_after_d AFTER DELETE ON ref_transformation_parameters FOR EACH ROW INSERT 
INTO 
    ref_transformation_parameters_hist 
    (
        transformation ,
        parameter_name ,
        hist_date_insert ,
        parameter_value_varchar ,
        parameter_value_datetime ,
        parameter_value_numeric ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        OLD.transformation ,
        OLD.parameter_name ,
        NOW() ,
        OLD.parameter_value_varchar ,
        OLD.parameter_value_datetime ,
        OLD.parameter_value_numeric ,
        OLD.description ,
        'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `etl_log_channel`
--

DROP TABLE IF EXISTS `etl_log_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etl_log_channel` (
  `ID_BATCH` int(11) DEFAULT NULL,
  `CHANNEL_ID` varchar(255) DEFAULT NULL,
  `LOG_DATE` datetime DEFAULT NULL,
  `LOGGING_OBJECT_TYPE` varchar(255) DEFAULT NULL,
  `OBJECT_NAME` varchar(255) DEFAULT NULL,
  `OBJECT_COPY` varchar(255) DEFAULT NULL,
  `REPOSITORY_DIRECTORY` varchar(255) DEFAULT NULL,
  `FILENAME` varchar(255) DEFAULT NULL,
  `OBJECT_ID` varchar(255) DEFAULT NULL,
  `OBJECT_REVISION` varchar(255) DEFAULT NULL,
  `PARENT_CHANNEL_ID` varchar(255) DEFAULT NULL,
  `ROOT_CHANNEL_ID` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `vw_stg_management_link_attributes_all_columns_filled`
--

DROP TABLE IF EXISTS `vw_stg_management_link_attributes_all_columns_filled`;
/*!50001 DROP VIEW IF EXISTS `vw_stg_management_link_attributes_all_columns_filled`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_stg_management_link_attributes_all_columns_filled` (
  `link_name` varchar(128),
  `source_concat` varchar(256),
  `ind_current` int(11),
  `attribute_number` int(11),
  `attribute_source_column` varchar(128),
  `attribute_target_column` varchar(128)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ref_processing_stages_hist`
--

DROP TABLE IF EXISTS `ref_processing_stages_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_processing_stages_hist` (
  `id_prcstg` tinyint(4) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `description` varchar(128) NOT NULL,
  `dml_operation` char(1) NOT NULL,
  PRIMARY KEY (`id_prcstg`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_source_tables`
--

DROP TABLE IF EXISTS `ref_source_tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_source_tables` (
  `id_srctab` int(11) NOT NULL AUTO_INCREMENT,
  `id_srcsys` tinyint(4) NOT NULL,
  `table_name` varchar(256) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `staging_table_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_srctab`) USING BTREE,
  KEY `id_srcsys` (`id_srcsys`) USING BTREE,
  CONSTRAINT `ref_source_tables_ibfk_1` FOREIGN KEY (`id_srcsys`) REFERENCES `ref_source_systems` (`id_srcsys`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_srctab_after_i AFTER INSERT ON ref_source_tables FOR EACH ROW INSERT 
INTO 
    ref_source_tables_hist 
    (
        id_srctab ,
        hist_date_insert ,
        id_srcsys ,
        table_name ,
        description, 
        staging_table_name,
        dml_operation
    ) 
    VALUES 
    (
        NEW.id_srctab ,
        NOW() ,
        NEW.id_srcsys ,
        NEW.table_name ,
        NEW.description, 
        NEW.staging_table_name,
        'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_srctab_after_u AFTER UPDATE ON ref_source_tables FOR EACH ROW INSERT 
INTO 
    ref_source_tables_hist 
    (
        id_srctab ,
        hist_date_insert ,
        id_srcsys ,
        table_name ,
        description, 
        staging_table_name,
        dml_operation
    ) 
    VALUES 
    (
        NEW.id_srctab ,
        NOW() ,
        NEW.id_srcsys ,
        NEW.table_name ,
        NEW.description, 
        NEW.staging_table_name,
        'U'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_srctab_after_d AFTER DELETE ON ref_source_tables FOR EACH ROW INSERT 
INTO 
    ref_source_tables_hist 
    (
        id_srctab ,
        hist_date_insert ,
        id_srcsys ,
        table_name ,
        description, 
        staging_table_name,
        dml_operation
    ) 
    VALUES 
    (
        OLD.id_srctab ,
        NOW() ,
        OLD.id_srcsys ,
        OLD.table_name ,
        OLD.description, 
        OLD.staging_table_name,
        'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ref_statuses_hist`
--

DROP TABLE IF EXISTS `ref_statuses_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_statuses_hist` (
  `id_status` tinyint(4) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `dml_operation` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_status`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_processing_stages`
--

DROP TABLE IF EXISTS `ref_processing_stages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_processing_stages` (
  `id_prcstg` tinyint(4) NOT NULL,
  `description` varchar(128) NOT NULL,
  PRIMARY KEY (`id_prcstg`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_prcstg_after_i AFTER INSERT ON ref_processing_stages FOR EACH ROW INSERT 
INTO 

    ref_processing_stages_hist 
    (
        id_prcstg ,
        hist_date_insert ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        NEW.id_prcstg ,
        NOW() ,
        NEW.description ,
        'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_prcstg_after_u AFTER UPDATE ON ref_processing_stages FOR EACH ROW INSERT 
INTO 
    ref_processing_stages_hist 
    (
        id_prcstg ,
        hist_date_insert ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        NEW.id_prcstg ,
        NOW() ,
        NEW.description ,
        'U'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_prcstg_after_d AFTER DELETE ON ref_processing_stages FOR EACH ROW INSERT 
INTO 
    ref_processing_stages_hist 
    (
        id_prcstg ,
        hist_date_insert ,
        description ,
        dml_operation
    ) 
    VALUES 
    (
        OLD.id_prcstg ,
        NOW() ,
        OLD.description ,
        'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ref_data_vault_link_attributes`
--

DROP TABLE IF EXISTS `ref_data_vault_link_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_link_attributes` (
  `id_data_vault_link_attribute` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_vault_link` int(11) DEFAULT NULL,
  `record_source_id` int(11) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `link_attributes` varchar(8092) DEFAULT NULL,
  `link_attributes_concat` varchar(8092) DEFAULT NULL,
  `link_attributes_concat_dv` varchar(8092) DEFAULT NULL,
  `link_attributes_dv` varchar(8092) DEFAULT NULL,
  PRIMARY KEY (`id_data_vault_link_attribute`) USING BTREE,
  KEY `fk_ref_dv_link_attributes_links` (`id_data_vault_link`) USING BTREE,
  CONSTRAINT `fk_ref_dv_link_attributes_links` FOREIGN KEY (`id_data_vault_link`) REFERENCES `ref_data_vault_links` (`id_data_vault_link`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_dv_link_attributes_after_i AFTER INSERT ON ref_data_vault_link_attributes FOR EACH ROW INSERT 
INTO 
    ref_data_vault_link_attributes_hist 
    (
      id_data_vault_link_attribute
     ,hist_date_insert 
     ,id_data_vault_link
     ,record_source_id            
     ,ind_current
     ,link_attributes
     ,link_attributes_concat
     ,link_attributes_concat_dv
     ,link_attributes_dv
     ,dml_operation
    ) 
    VALUES 
    (
      NEW.id_data_vault_link_attribute
     ,NOW() 
     ,NEW.id_data_vault_link
     ,NEW.record_source_id            
     ,NEW.ind_current
     ,NEW.link_attributes
     ,NEW.link_attributes_concat
     ,NEW.link_attributes_concat_dv
     ,NEW.link_attributes_dv
     , 'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_dv_link_attributes_after_u AFTER UPDATE ON ref_data_vault_link_attributes FOR EACH ROW
IF     ifnull(NEW.id_data_vault_link,-1)         <> ifnull(OLD.id_data_vault_link,-1)
or     ifnull(NEW.record_source_id,-1)           <> ifnull(OLD.record_source_id,-1)
or     ifnull(NEW.ind_current,-1)                <> ifnull(OLD.ind_current,-1)
or     ifnull(NEW.link_attributes,'')            <> ifnull(OLD.link_attributes,'')
or     ifnull(NEW.link_attributes_concat,'')     <> ifnull(OLD.link_attributes_concat,'')
or     ifnull(NEW.link_attributes_concat_dv,'')  <> ifnull(OLD.link_attributes_concat_dv,'')
or     ifnull(NEW.link_attributes_dv,'')         <> ifnull(OLD.link_attributes_dv,'')
THEN
INSERT 
INTO 
    ref_data_vault_link_attributes_hist 
    (
      id_data_vault_link_attribute
     ,hist_date_insert 
     ,id_data_vault_link
     ,record_source_id            
     ,ind_current
     ,link_attributes
     ,link_attributes_concat
     ,link_attributes_concat_dv
     ,link_attributes_dv
     ,dml_operation
    ) 
    VALUES 
    (
      NEW.id_data_vault_link_attribute
     ,NOW() 
     ,NEW.id_data_vault_link
     ,NEW.record_source_id            
     ,NEW.ind_current
     ,NEW.link_attributes
     ,NEW.link_attributes_concat
     ,NEW.link_attributes_concat_dv
     ,NEW.link_attributes_dv
     , 'U'
    );
END IF */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_dv_link_attributes_after_d AFTER DELETE ON ref_data_vault_link_attributes FOR EACH ROW INSERT 
INTO 
    ref_data_vault_link_attributes_hist 
    (
      id_data_vault_link_attribute
     ,hist_date_insert 
     ,id_data_vault_link
     ,record_source_id            
     ,ind_current
     ,link_attributes
     ,link_attributes_concat
     ,link_attributes_concat_dv
     ,link_attributes_dv
     ,dml_operation
    ) 
    VALUES 
    (
      OLD.id_data_vault_link_attribute
     ,NOW() 
     ,OLD.id_data_vault_link
     ,OLD.record_source_id            
     ,OLD.ind_current
     ,OLD.link_attributes
     ,OLD.link_attributes_concat
     ,OLD.link_attributes_concat_dv
     ,OLD.link_attributes_dv
     , 'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ref_data_vault_link_satellites`
--

DROP TABLE IF EXISTS `ref_data_vault_link_satellites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_link_satellites` (
  `id_data_vault_link_sat` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_vault_link` int(11) DEFAULT NULL,
  `sat_name` varchar(128) DEFAULT NULL,
  `sat_key` varchar(128) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `sat_source_hub_1_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_2_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_3_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_4_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_5_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_6_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_7_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_8_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_9_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_10_business_key` varchar(128) DEFAULT NULL,
  `sat_lnk_key_attributes_concat` varchar(8092) DEFAULT NULL,
  `sat_attributes` varchar(8092) DEFAULT NULL,
  `sat_attributes_dv` varchar(8092) DEFAULT NULL,
  `sat_attributes_concat` varchar(8092) DEFAULT NULL,
  `sat_attributes_concat_dv` varchar(8092) DEFAULT NULL,
  `record_source_id` int(11) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_data_vault_link_sat`) USING BTREE,
  KEY `fk_ref_dv_links_satellites_links` (`id_data_vault_link`) USING BTREE,
  KEY `fk_ref_dv_link_satellites_source_tables` (`record_source_id`) USING BTREE,
  CONSTRAINT `fk_ref_dv_links_satellites_links` FOREIGN KEY (`id_data_vault_link`) REFERENCES `ref_data_vault_links` (`id_data_vault_link`),
  CONSTRAINT `fk_ref_dv_link_satellites_source_tables` FOREIGN KEY (`record_source_id`) REFERENCES `ref_source_tables` (`id_srctab`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_dv_link_sat_after_i AFTER INSERT ON ref_data_vault_link_satellites FOR EACH ROW INSERT 
INTO 
    ref_data_vault_link_satellites_hist 
    (
      id_data_vault_link_sat
     ,id_data_vault_link
     ,hist_date_insert 
     ,sat_name
     ,sat_key
     ,description                 
     ,sat_source_hub_1_business_key
     ,sat_source_hub_2_business_key
     ,sat_source_hub_3_business_key
     ,sat_source_hub_4_business_key
     ,sat_source_hub_5_business_key
     ,sat_source_hub_6_business_key
     ,sat_source_hub_7_business_key
     ,sat_source_hub_8_business_key
     ,sat_source_hub_9_business_key
     ,sat_source_hub_10_business_key
     ,sat_lnk_key_attributes_concat
     ,sat_attributes
     ,sat_attributes_dv
     ,sat_attributes_concat
     ,sat_attributes_concat_dv
     ,record_source_id
     ,ind_current
     ,dml_operation
    ) 
    VALUES 
    (
      NEW.id_data_vault_link_sat
     ,NEW.id_data_vault_link
     ,NOW() 
     ,NEW.sat_name
     ,NEW.sat_key
     ,NEW.description                 
     ,NEW.sat_source_hub_1_business_key
     ,NEW.sat_source_hub_2_business_key
     ,NEW.sat_source_hub_3_business_key
     ,NEW.sat_source_hub_4_business_key
     ,NEW.sat_source_hub_5_business_key
     ,NEW.sat_source_hub_6_business_key
     ,NEW.sat_source_hub_7_business_key
     ,NEW.sat_source_hub_8_business_key
     ,NEW.sat_source_hub_9_business_key
     ,NEW.sat_source_hub_10_business_key
     ,NEW.sat_lnk_key_attributes_concat
     ,NEW.sat_attributes
     ,NEW.sat_attributes_dv
     ,NEW.sat_attributes_concat
     ,NEW.sat_attributes_concat_dv
     ,NEW.record_source_id
     ,NEW.ind_current
     ,'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_dv_link_sat_after_u AFTER UPDATE ON ref_data_vault_link_satellites FOR EACH ROW
IF     ifnull(NEW.id_data_vault_link,-1)              <> ifnull(OLD.id_data_vault_link,-1)
or     ifnull(NEW.sat_name,'')                        <> ifnull(OLD.sat_name,'')
or     ifnull(NEW.sat_key,'')                         <> ifnull(OLD.sat_key,'')
or     ifnull(NEW.description,'')                     <> ifnull(OLD.description,'')
or     ifnull(NEW.sat_source_hub_1_business_key,'')   <> ifnull(OLD.sat_source_hub_1_business_key,'')
or     ifnull(NEW.sat_source_hub_2_business_key,'')   <> ifnull(OLD.sat_source_hub_2_business_key,'')
or     ifnull(NEW.sat_source_hub_3_business_key,'')   <> ifnull(OLD.sat_source_hub_3_business_key,'')
or     ifnull(NEW.sat_source_hub_4_business_key,'')   <> ifnull(OLD.sat_source_hub_4_business_key,'')
or     ifnull(NEW.sat_source_hub_5_business_key,'')   <> ifnull(OLD.sat_source_hub_5_business_key,'')
or     ifnull(NEW.sat_source_hub_6_business_key,'')   <> ifnull(OLD.sat_source_hub_6_business_key,'')
or     ifnull(NEW.sat_source_hub_7_business_key,'')   <> ifnull(OLD.sat_source_hub_7_business_key,'')
or     ifnull(NEW.sat_source_hub_8_business_key,'')   <> ifnull(OLD.sat_source_hub_8_business_key,'')
or     ifnull(NEW.sat_source_hub_9_business_key,'')   <> ifnull(OLD.sat_source_hub_9_business_key,'')
or     ifnull(NEW.sat_source_hub_10_business_key,'')  <> ifnull(OLD.sat_source_hub_10_business_key,'')
or     ifnull(NEW.sat_lnk_key_attributes_concat,'')   <> ifnull(OLD.sat_lnk_key_attributes_concat,'')
or     ifnull(NEW.sat_attributes,'')                  <> ifnull(OLD.sat_attributes,'')
or     ifnull(NEW.sat_attributes_dv,'')               <> ifnull(OLD.sat_attributes_dv,'')
or     ifnull(NEW.sat_attributes_concat,'')           <> ifnull(OLD.sat_attributes_concat,'')
or     ifnull(NEW.sat_attributes_concat_dv,'')        <> ifnull(OLD.sat_attributes_concat_dv,'')
or     ifnull(NEW.record_source_id,-1)                <> ifnull(OLD.record_source_id,-1)
or     ifnull(NEW.ind_current,-1)                     <> ifnull(OLD.ind_current,-1)
THEN INSERT 
INTO 
    ref_data_vault_link_satellites_hist 
    (
      id_data_vault_link_sat
     ,id_data_vault_link
     ,hist_date_insert 
     ,sat_name
     ,sat_key
     ,description                 
     ,sat_source_hub_1_business_key
     ,sat_source_hub_2_business_key
     ,sat_source_hub_3_business_key
     ,sat_source_hub_4_business_key
     ,sat_source_hub_5_business_key
     ,sat_source_hub_6_business_key
     ,sat_source_hub_7_business_key
     ,sat_source_hub_8_business_key
     ,sat_source_hub_9_business_key
     ,sat_source_hub_10_business_key
     ,sat_lnk_key_attributes_concat
     ,sat_attributes
     ,sat_attributes_dv
     ,sat_attributes_concat
     ,sat_attributes_concat_dv
     ,record_source_id
     ,ind_current
     ,dml_operation
    ) 
    VALUES 
    (
      NEW.id_data_vault_link_sat
     ,NEW.id_data_vault_link
     ,NOW() 
     ,NEW.sat_name
     ,NEW.sat_key
     ,NEW.description                 
     ,NEW.sat_source_hub_1_business_key
     ,NEW.sat_source_hub_2_business_key
     ,NEW.sat_source_hub_3_business_key
     ,NEW.sat_source_hub_4_business_key
     ,NEW.sat_source_hub_5_business_key
     ,NEW.sat_source_hub_6_business_key
     ,NEW.sat_source_hub_7_business_key
     ,NEW.sat_source_hub_8_business_key
     ,NEW.sat_source_hub_9_business_key
     ,NEW.sat_source_hub_10_business_key
     ,NEW.sat_lnk_key_attributes_concat
     ,NEW.sat_attributes
     ,NEW.sat_attributes_dv
     ,NEW.sat_attributes_concat
     ,NEW.sat_attributes_concat_dv
     ,NEW.record_source_id
     ,NEW.ind_current
     ,'U'
    );
END IF */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_dv_link_sat_after_d AFTER DELETE ON ref_data_vault_link_satellites FOR EACH ROW INSERT 
INTO 
    ref_data_vault_link_satellites_hist 
    (
      id_data_vault_link_sat
     ,id_data_vault_link
     ,hist_date_insert 
     ,sat_name
     ,sat_key
     ,description                 
     ,sat_source_hub_1_business_key
     ,sat_source_hub_2_business_key
     ,sat_source_hub_3_business_key
     ,sat_source_hub_4_business_key
     ,sat_source_hub_5_business_key
     ,sat_source_hub_6_business_key
     ,sat_source_hub_7_business_key
     ,sat_source_hub_8_business_key
     ,sat_source_hub_9_business_key
     ,sat_source_hub_10_business_key
     ,sat_lnk_key_attributes_concat
     ,sat_attributes
     ,sat_attributes_dv
     ,sat_attributes_concat
     ,sat_attributes_concat_dv
     ,record_source_id
     ,ind_current
     ,dml_operation
    ) 
    VALUES 
    (
      OLD.id_data_vault_link_sat
     ,OLD.id_data_vault_link
     ,NOW() 
     ,OLD.sat_name
     ,OLD.sat_key
     ,OLD.description                 
     ,OLD.sat_source_hub_1_business_key
     ,OLD.sat_source_hub_2_business_key
     ,OLD.sat_source_hub_3_business_key
     ,OLD.sat_source_hub_4_business_key
     ,OLD.sat_source_hub_5_business_key
     ,OLD.sat_source_hub_6_business_key
     ,OLD.sat_source_hub_7_business_key
     ,OLD.sat_source_hub_8_business_key
     ,OLD.sat_source_hub_9_business_key
     ,OLD.sat_source_hub_10_business_key
     ,OLD.sat_lnk_key_attributes_concat
     ,OLD.sat_attributes
     ,OLD.sat_attributes_dv
     ,OLD.sat_attributes_concat
     ,OLD.sat_attributes_concat_dv
     ,OLD.record_source_id
     ,OLD.ind_current
     ,'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `etl_log_job_entry`
--

DROP TABLE IF EXISTS `etl_log_job_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etl_log_job_entry` (
  `ID_BATCH` int(11) DEFAULT NULL,
  `CHANNEL_ID` varchar(255) DEFAULT NULL,
  `LOG_DATE` datetime DEFAULT NULL,
  `TRANSNAME` varchar(255) DEFAULT NULL,
  `STEPNAME` varchar(255) DEFAULT NULL,
  `LINES_READ` bigint(20) DEFAULT NULL,
  `LINES_WRITTEN` bigint(20) DEFAULT NULL,
  `LINES_UPDATED` bigint(20) DEFAULT NULL,
  `LINES_INPUT` bigint(20) DEFAULT NULL,
  `LINES_OUTPUT` bigint(20) DEFAULT NULL,
  `LINES_REJECTED` bigint(20) DEFAULT NULL,
  `ERRORS` bigint(20) DEFAULT NULL,
  `RESULT` char(1) DEFAULT NULL,
  `NR_RESULT_ROWS` bigint(20) DEFAULT NULL,
  `NR_RESULT_FILES` bigint(20) DEFAULT NULL,
  `LOG_FIELD` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_system_parameters_hist`
--

DROP TABLE IF EXISTS `ref_system_parameters_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_system_parameters_hist` (
  `parameter_name` varchar(128) NOT NULL,
  `hist_date_insert` datetime NOT NULL,
  `parameter_value_varchar` varchar(128) DEFAULT NULL,
  `parameter_value_datetime` datetime DEFAULT NULL,
  `parameter_value_numeric` decimal(12,2) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `dml_operation` char(1) NOT NULL,
  PRIMARY KEY (`parameter_name`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_connections`
--

DROP TABLE IF EXISTS `ref_connections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_connections` (
  `id_connection` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `type` varchar(32) DEFAULT NULL,
  `description` varchar(128) NOT NULL,
  `mysql_host_name` varchar(128) DEFAULT NULL,
  `mysql_database_name` varchar(128) DEFAULT NULL,
  `mysql_port_number` int(11) DEFAULT NULL,
  `mysql_user_name` varchar(128) DEFAULT NULL,
  `mysql_password` varchar(128) DEFAULT NULL,
  `sqlserver_host_name` varchar(128) DEFAULT NULL,
  `sqlserver_database_name` varchar(128) DEFAULT NULL,
  `sqlserver_instance_name` varchar(128) DEFAULT NULL,
  `sqlserver_port_number` int(11) DEFAULT NULL,
  `sqlserver_user_name` varchar(128) DEFAULT NULL,
  `sqlserver_password` varchar(128) DEFAULT NULL,
  `oracle_host_name` varchar(128) DEFAULT NULL,
  `oracle_database_name` varchar(128) DEFAULT NULL,
  `oracle_port_number` int(11) DEFAULT NULL,
  `oracle_user_name` varchar(128) DEFAULT NULL,
  `oracle_password` varchar(128) DEFAULT NULL,
  `postgresql_host_name` varchar(128) DEFAULT NULL,
  `postgresql_database_name` varchar(128) DEFAULT NULL,
  `postgresql_port_number` int(11) DEFAULT NULL,
  `postgresql_user_name` varchar(128) DEFAULT NULL,
  `postgresql_password` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_connection`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_conn_after_i AFTER INSERT ON ref_connections FOR EACH ROW INSERT 
INTO 
    ref_connections_hist 
    (
  id_connection ,
  hist_date_insert ,
  name,
  type,
  description,
  mysql_host_name,
  mysql_database_name,
  mysql_port_number,
  mysql_user_name,
  mysql_password,
  sqlserver_host_name,
  sqlserver_database_name,
  sqlserver_instance_name,  
  sqlserver_port_number,
  sqlserver_user_name,
  sqlserver_password,
  oracle_host_name,
  oracle_database_name,
  oracle_port_number,
  oracle_user_name,
  oracle_password,
  postgresql_host_name,
  postgresql_database_name,
  postgresql_port_number,
  postgresql_user_name,
  postgresql_password,
  dml_operation
    ) 
    VALUES 
    (
        NEW.id_connection ,
        NOW() ,
  NEW.name,
  NEW.type,
  NEW.description,
  NEW.mysql_host_name,
  NEW.mysql_database_name,
  NEW.mysql_port_number,
  NEW.mysql_user_name,
  NEW.mysql_password,
  NEW.sqlserver_host_name,
  NEW.sqlserver_database_name,
  NEW.sqlserver_instance_name,  
  NEW.sqlserver_port_number,
  NEW.sqlserver_user_name,
  NEW.sqlserver_password,
  NEW.oracle_host_name,
  NEW.oracle_database_name,
  NEW.oracle_port_number,
  NEW.oracle_user_name,
  NEW.oracle_password,
  NEW.postgresql_host_name,
  NEW.postgresql_database_name,
  NEW.postgresql_port_number,
  NEW.postgresql_user_name,
  NEW.postgresql_password,
        'I'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_conn_after_u AFTER UPDATE ON ref_connections FOR EACH ROW INSERT 
INTO 
    ref_connections_hist 
    (
  id_connection ,
  hist_date_insert ,
  name,
  type,
  description,
  mysql_host_name,
  mysql_database_name,
  mysql_port_number,
  mysql_user_name,
  mysql_password,
  sqlserver_host_name,
  sqlserver_database_name,
  sqlserver_instance_name,  
  sqlserver_port_number,
  sqlserver_user_name,
  sqlserver_password,
  oracle_host_name,
  oracle_database_name,
  oracle_port_number,
  oracle_user_name,
  oracle_password,
  postgresql_host_name,
  postgresql_database_name,
  postgresql_port_number,
  postgresql_user_name,
  postgresql_password,
  dml_operation
    ) 
    VALUES 
    (
        NEW.id_connection ,
        NOW() ,
  NEW.name,
  NEW.type,
  NEW.description,
  NEW.mysql_host_name,
  NEW.mysql_database_name,
  NEW.mysql_port_number,
  NEW.mysql_user_name,
  NEW.mysql_password,
  NEW.sqlserver_host_name,
  NEW.sqlserver_database_name,
  NEW.sqlserver_instance_name,  
  NEW.sqlserver_port_number,
  NEW.sqlserver_user_name,
  NEW.sqlserver_password,
  NEW.oracle_host_name,
  NEW.oracle_database_name,
  NEW.oracle_port_number,
  NEW.oracle_user_name,
  NEW.oracle_password,
  NEW.postgresql_host_name,
  NEW.postgresql_database_name,
  NEW.postgresql_port_number,
  NEW.postgresql_user_name,
  NEW.postgresql_password,
        'U'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER  trg_ref_conn_after_d AFTER DELETE ON ref_connections FOR EACH ROW INSERT 
INTO 
    ref_connections_hist 
    (
  id_connection ,
  hist_date_insert ,
  name,
  type,
  description,
  mysql_host_name,
  mysql_database_name,
  mysql_port_number,
  mysql_user_name,
  mysql_password,
  sqlserver_host_name,
  sqlserver_database_name,
  sqlserver_instance_name,  
  sqlserver_port_number,
  sqlserver_user_name,
  sqlserver_password,
  oracle_host_name,
  oracle_database_name,
  oracle_port_number,
  oracle_user_name,
  oracle_password,
  postgresql_host_name,
  postgresql_database_name,
  postgresql_port_number,
  postgresql_user_name,
  postgresql_password,
  dml_operation
    ) 
    VALUES 
    (
        OLD.id_connection ,
        NOW() ,
  OLD.name,
  OLD.type,
  OLD.description,
  OLD.mysql_host_name,
  OLD.mysql_database_name,
  OLD.mysql_port_number,
  OLD.mysql_user_name,
  OLD.mysql_password,
  OLD.sqlserver_host_name,
  OLD.sqlserver_database_name,
  OLD.sqlserver_instance_name,  
  OLD.sqlserver_port_number,
  OLD.sqlserver_user_name,
  OLD.sqlserver_password,
  OLD.oracle_host_name,
  OLD.oracle_database_name,
  OLD.oracle_port_number,
  OLD.oracle_user_name,
  OLD.oracle_password,
  OLD.postgresql_host_name,
  OLD.postgresql_database_name,
  OLD.postgresql_port_number,
  OLD.postgresql_user_name,
  OLD.postgresql_password,
        'D'
    ) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ref_data_vault_link_satellites_hist`
--

DROP TABLE IF EXISTS `ref_data_vault_link_satellites_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_data_vault_link_satellites_hist` (
  `id_data_vault_link_sat` int(11) NOT NULL,
  `id_data_vault_link` int(11) DEFAULT NULL,
  `hist_date_insert` datetime NOT NULL,
  `sat_name` varchar(128) DEFAULT NULL,
  `sat_key` varchar(128) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `sat_source_hub_1_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_2_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_3_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_4_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_5_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_6_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_7_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_8_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_9_business_key` varchar(128) DEFAULT NULL,
  `sat_source_hub_10_business_key` varchar(128) DEFAULT NULL,
  `sat_lnk_key_attributes_concat` varchar(8092) DEFAULT NULL,
  `sat_attributes` varchar(8092) DEFAULT NULL,
  `sat_attributes_dv` varchar(8092) DEFAULT NULL,
  `sat_attributes_concat` varchar(8092) DEFAULT NULL,
  `sat_attributes_concat_dv` varchar(8092) DEFAULT NULL,
  `record_source_id` int(11) DEFAULT NULL,
  `ind_current` int(11) DEFAULT NULL,
  `dml_operation` char(1) NOT NULL,
  PRIMARY KEY (`id_data_vault_link_sat`,`hist_date_insert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `vw_stg_management_link_satellites_all_columns_filled`
--

/*!50001 DROP TABLE IF EXISTS `vw_stg_management_link_satellites_all_columns_filled`*/;
/*!50001 DROP VIEW IF EXISTS `vw_stg_management_link_satellites_all_columns_filled`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_stg_management_link_satellites_all_columns_filled` AS select `stg_sat_2`.`sat_name` AS `sat_name`,`stg_sat_2`.`sat_key` AS `sat_key`,`stg_sat_2`.`sat_description` AS `sat_description`,`stg_sat_2`.`sat_link` AS `sat_link`,`stg_sat_2`.`source_concat` AS `source_concat`,`stg_sat_2`.`ind_current` AS `ind_current`,`stg_sat_2`.`source_hub_1_business_key` AS `source_hub_1_business_key`,`stg_sat_2`.`source_hub_2_business_key` AS `source_hub_2_business_key`,`stg_sat_2`.`source_hub_3_business_key` AS `source_hub_3_business_key`,`stg_sat_2`.`source_hub_4_business_key` AS `source_hub_4_business_key`,`stg_sat_2`.`source_hub_5_business_key` AS `source_hub_5_business_key`,`stg_sat_2`.`source_hub_6_business_key` AS `source_hub_6_business_key`,`stg_sat_2`.`source_hub_7_business_key` AS `source_hub_7_business_key`,`stg_sat_2`.`source_hub_8_business_key` AS `source_hub_8_business_key`,`stg_sat_2`.`source_hub_9_business_key` AS `source_hub_9_business_key`,`stg_sat_2`.`source_hub_10_business_key` AS `source_hub_10_business_key`,`stg_sat_2`.`source_lnk_key_attribute_1` AS `source_lnk_key_attribute_1`,`stg_sat_2`.`source_lnk_key_attribute_2` AS `source_lnk_key_attribute_2`,`stg_sat_2`.`source_lnk_key_attribute_3` AS `source_lnk_key_attribute_3`,`stg_sat_2`.`source_lnk_key_attribute_4` AS `source_lnk_key_attribute_4`,`stg_sat_2`.`source_lnk_key_attribute_5` AS `source_lnk_key_attribute_5`,`stg_sat`.`attribute_number` AS `attribute_number`,`stg_sat`.`attribute_source_column` AS `attribute_source_column`,`stg_sat`.`attribute_target_column` AS `attribute_target_column` from (`stg_management_link_satellites` `stg_sat` join `stg_management_link_satellites` `stg_sat_2` on((`stg_sat_2`.`sheet_row_number` = (select max(`stg_sat_3`.`sheet_row_number`) from `stg_management_link_satellites` `stg_sat_3` where ((`stg_sat_3`.`sheet_row_number` <= `stg_sat`.`sheet_row_number`) and (`stg_sat_3`.`sat_name` is not null)))))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_inst_transformation_parameters_pivot_run`
--

/*!50001 DROP TABLE IF EXISTS `vw_inst_transformation_parameters_pivot_run`*/;
/*!50001 DROP VIEW IF EXISTS `vw_inst_transformation_parameters_pivot_run`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_inst_transformation_parameters_pivot_run` AS select `trfpar`.`transformation` AS `transformation`,`spar`.`repodir_dimensional` AS `repodir_dimensional`,`arun`.`id_run` AS `id_run`,cast((((year(`arun`.`date_start`) * 10000) + (month(`arun`.`date_start`) * 100)) + dayofmonth(`arun`.`date_start`)) as datetime) AS `hist_datum` from ((`inst_actual_runs` `arun` join `vw_ref_system_parameters_pivot` `spar`) join `vw_ref_transformation_parameters_pivot` `trfpar`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_ref_transformation_parameters_pivot`
--

/*!50001 DROP TABLE IF EXISTS `vw_ref_transformation_parameters_pivot`*/;
/*!50001 DROP VIEW IF EXISTS `vw_ref_transformation_parameters_pivot`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_ref_transformation_parameters_pivot` AS select `rtrf`.`transformation` AS `transformation` from (`ref_transformations` `rtrf` left join `ref_transformation_parameters` `trpm` on((`rtrf`.`transformation` = `trpm`.`transformation`))) group by `rtrf`.`transformation` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_stg_management_satellites_all_columns_filled`
--

/*!50001 DROP TABLE IF EXISTS `vw_stg_management_satellites_all_columns_filled`*/;
/*!50001 DROP VIEW IF EXISTS `vw_stg_management_satellites_all_columns_filled`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_stg_management_satellites_all_columns_filled` AS select `stg_sat_2`.`sat_name` AS `sat_name`,`stg_sat_2`.`sat_key` AS `sat_key`,`stg_sat_2`.`sat_description` AS `sat_description`,`stg_sat_2`.`sat_hub` AS `sat_hub`,`stg_sat_2`.`source_concat` AS `source_concat`,`stg_sat_2`.`ind_current` AS `ind_current`,`stg_sat_2`.`source_hub_business_key` AS `source_hub_business_key`,`stg_sat`.`attribute_number` AS `attribute_number`,`stg_sat`.`attribute_source_column` AS `attribute_source_column`,`stg_sat`.`attribute_target_column` AS `attribute_target_column`,`stg_sat`.`ind_multiactive_extra_key_column` AS `ind_multiactive_extra_key_column` from (`stg_management_satellites` `stg_sat` join `stg_management_satellites` `stg_sat_2` on((`stg_sat_2`.`sheet_row_number` = (select max(`stg_sat_3`.`sheet_row_number`) from `stg_management_satellites` `stg_sat_3` where ((`stg_sat_3`.`sheet_row_number` <= `stg_sat`.`sheet_row_number`) and (`stg_sat_3`.`sat_name` is not null)))))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_ref_system_parameters_pivot`
--

/*!50001 DROP TABLE IF EXISTS `vw_ref_system_parameters_pivot`*/;
/*!50001 DROP VIEW IF EXISTS `vw_ref_system_parameters_pivot`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_ref_system_parameters_pivot` AS select max((case when (`ref_system_parameters`.`parameter_name` = 'repodir_dimensional') then `ref_system_parameters`.`parameter_value_varchar` else NULL end)) AS `repodir_dimensional` from `ref_system_parameters` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_stg_management_link_attributes_all_columns_filled`
--

/*!50001 DROP TABLE IF EXISTS `vw_stg_management_link_attributes_all_columns_filled`*/;
/*!50001 DROP VIEW IF EXISTS `vw_stg_management_link_attributes_all_columns_filled`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_stg_management_link_attributes_all_columns_filled` AS select `stg_latt_2`.`link_name` AS `link_name`,`stg_latt_2`.`source_concat` AS `source_concat`,`stg_latt_2`.`ind_current` AS `ind_current`,`stg_latt`.`attribute_number` AS `attribute_number`,`stg_latt`.`attribute_source_column` AS `attribute_source_column`,`stg_latt`.`attribute_target_column` AS `attribute_target_column` from (`stg_management_link_attributes` `stg_latt` join `stg_management_link_attributes` `stg_latt_2` on((`stg_latt_2`.`sheet_row_number` = (select max(`stg_latt_3`.`sheet_row_number`) from `stg_management_link_attributes` `stg_latt_3` where ((`stg_latt_3`.`sheet_row_number` <= `stg_latt`.`sheet_row_number`) and (`stg_latt_3`.`link_name` is not null)))))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping routines for database 'pdi_meta_dv_demo'
--
/*!50003 DROP PROCEDURE IF EXISTS `prc_create_error_table` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `prc_create_error_table`(in p_table_name varchar(128),in p_schema_name varchar(128))
BEGIN

DECLARE done INT DEFAULT 0;

DECLARE coldef VARCHAR(128);
DECLARE cur_col CURSOR FOR 
select concat(case when ordinal_position = 1 then '' else ',' end,column_name,' '
,      case
          when column_name = 'load_dts'         then ' datetime '
          when column_name = 'last_seen_dts'    then ' datetime '
          when column_name = 'record_source_id' then ' int '
          when column_key  = 'PRI'              then ' int default -1'
          else                                       ' text '
       end
       ) as attribute
from   information_schema.columns
where  table_name = p_table_name
and    table_schema = p_schema_name
order  by ordinal_position;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

SET @stmt_def = concat('create table if not exists ',p_schema_name,'.',p_table_name,'_err (');

open cur_col;
read_loop: LOOP
    FETCH cur_col INTO coldef; 
    IF done THEN LEAVE read_loop;
    END IF;
    set @stmt_def = concat(@stmt_def,coldef);
  END LOOP;
  CLOSE cur_col;

set @stmt_def = concat(@stmt_def,',etl_err_date timestamp default current_timestamp,etl_id_run int,etl_err_noe int, etl_err_desc varchar(512),etl_err_col varchar(256),etl_err_cod varchar(256) ) engine=MyISAM default charset latin1');


PREPARE stmt_name FROM @stmt_def;
EXECUTE stmt_name;
DEALLOCATE PREPARE stmt_name;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `prc_create_new_run` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `prc_create_new_run`(IN p_id_rtyp INTEGER, IN p_ind_restart INTEGER)
BEGIN
    DECLARE var_last_load_dts DATETIME;
    SELECT  load_dts FROM inst_actual_runs  INTO @var_last_load_dts;

    DELETE FROM inst_actual_runs;    
    IF p_ind_restart = 0 THEN INSERT INTO inst_actual_runs (  id_rtyp, id_status, date_start,   ind_restart,            load_dts  )
                                                    VALUES (p_id_rtyp,         1,      now(), p_ind_restart,               now()   );
                         ELSE INSERT INTO inst_actual_runs (  id_rtyp, id_status, date_start,   ind_restart,            load_dts  )
                                                    VALUES (p_id_rtyp,         1,      now(), p_ind_restart,    @var_last_load_dts );
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `prc_end_run` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `prc_end_run`(   IN p_id_status   INTEGER,   IN p_error_message VARCHAR(256))
BEGIN
    UPDATE  inst_actual_runs
    SET     date_end      = now()
    ,       id_status     = p_id_status
    ,       error_message = p_error_message;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `prc_log_dv_job_in_run` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `prc_log_dv_job_in_run`(   IN p_job                      VARCHAR(512)
,   IN p_transformation           VARCHAR(512)
,   IN p_start_end                VARCHAR(5)
,   IN p_record_source_id         INT 
,   IN p_source_order             INT
,   IN p_data_vault_object        VARCHAR(128)
,   IN p_data_vault_hub           VARCHAR(128) 
,   IN p_data_vault_hub_sat       VARCHAR(128)  
,   IN p_data_vault_link          VARCHAR(128) 
,   IN p_data_vault_link_sat      VARCHAR(128) 
,   IN p_data_vault_database_name VARCHAR(128)
,   IN p_data_vault_object_key    VARCHAR(128)
)
BEGIN
        DECLARE var_actual_run  INT;
        DECLARE var_num_records INT;
        DECLARE var_num_errors  INT;
        DECLARE var_use_index_hint VARCHAR(128);
        SELECT  id_run FROM inst_actual_runs  INTO @var_actual_run;

        select concat(' use index(',max(index_name),') ')
        from   information_schema.statistics
        where  table_name   =  p_data_vault_object
        and    table_schema =  p_data_vault_database_name
        and    column_name  =  p_data_vault_object_key
        and    seq_in_index = 1
        INTO   @var_use_index_hint;

        IF p_start_end = 'start' THEN

        SET @stmt_def = concat('select count(1) from ',p_data_vault_database_name,'.',p_data_vault_object,@var_use_index_hint,' into @var_num_records');

        PREPARE stmt_name FROM @stmt_def;
        EXECUTE stmt_name;
        DEALLOCATE PREPARE stmt_name;

        SET @stmt_def = concat('insert into inst_run_dv_jobs (id_run,job,transformation,date_start,record_source_id,source_order'
                              ,',data_vault_object,data_vault_hub,data_vault_hub_sat,data_vault_link,data_vault_link_sat'
                              ,',num_records_start)'
                              , ' values ('
                              ,  coalesce(@var_actual_run,0),',','''',p_job,'''',',','''',p_transformation,'''',',','now()',',',p_record_source_id,',',coalesce(p_source_order,1)
                              ,',','''',p_data_vault_object,''''
                              ,',','''',coalesce(p_data_vault_hub,'null'),''''
                              ,',','''',coalesce(p_data_vault_hub_sat,'null'),''''
                              ,',','''',coalesce(p_data_vault_link,'null'),''''
                              ,',','''',coalesce(p_data_vault_link_sat,'null'),''''
                              ,',',coalesce(@var_num_records,0),')'

                              );

        set @stmt_def = replace(@stmt_def,'''null''','null');

        PREPARE stmt_name FROM @stmt_def;
        EXECUTE stmt_name;
        DEALLOCATE PREPARE stmt_name;

        END IF;

        IF p_start_end = 'end' THEN

        SET @stmt_def = concat('select count(1) from ',p_data_vault_database_name,'.',p_data_vault_object,@var_use_index_hint,' into @var_num_records');

        PREPARE stmt_name FROM @stmt_def;
        EXECUTE stmt_name;
        DEALLOCATE PREPARE stmt_name;

        SET @stmt_def = concat('select count(1) from ',p_data_vault_database_name,'.',p_data_vault_object,'_err where etl_id_run is NULL into @var_num_errors');

        PREPARE stmt_name FROM @stmt_def;
        EXECUTE stmt_name;
        DEALLOCATE PREPARE stmt_name;

        SET @stmt_def = concat('update inst_run_dv_jobs set num_records_end = ',@var_num_records,',num_errors = ',@var_num_errors,',date_end = ','now()'
                               ,' where id_run = ',coalesce(@var_actual_run,0)
                               ,' and    data_vault_object = ','''',p_data_vault_object,''''
                               ,' and    record_source_id  = ',p_record_source_id
                               ,' and    source_order      = ',coalesce(p_source_order,1) 
                               ,' and    date_end          is null');

        PREPARE stmt_name FROM @stmt_def;
        EXECUTE stmt_name;
        DEALLOCATE PREPARE stmt_name;          

        END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `prc_log_source_file_in_run` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `prc_log_source_file_in_run`(IN p_id_srcsys          TINYINT,   IN p_source_file        VARCHAR(256))
BEGIN
        DECLARE var_actual_run INT;SELECT  id_run FROM inst_actual_runs  INTO var_actual_run;INSERT INTO inst_run_source_files
                (id_run
                ,id_srcsys
                ,source_file
                )
        VALUES  (var_actual_run
                ,p_id_srcsys
                ,p_source_file
                );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `prc_log_transformation_in_run` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `prc_log_transformation_in_run`(   IN p_transformation     VARCHAR(512)
,   IN p_etl_id_job              INT
,   IN p_etl_id_batch            INT
,   IN p_record_source_id        INT 
,   IN p_data_vault_hub          VARCHAR(128) 
,   IN p_data_vault_hub_sat      VARCHAR(128)  
,   IN p_data_vault_link         VARCHAR(128) 
,   IN p_data_vault_link_sat     VARCHAR(128) 
)
BEGIN
        DECLARE var_actual_run INT;
        SELECT  id_run FROM inst_actual_runs  INTO var_actual_run;
        INSERT INTO inst_run_transformations
                (id_run
                ,transformation
                ,etl_id_job
                ,etl_id_batch
                ,etl_date_insert
                ,record_source_id
                ,data_vault_hub
                ,data_vault_hub_sat
                ,data_vault_link 
                ,data_vault_link_sat
                )
        VALUES  (var_actual_run
                ,p_transformation
                ,p_etl_id_job
                ,p_etl_id_batch
                ,now()
                ,p_record_source_id
                ,p_data_vault_hub
                ,p_data_vault_hub_sat
                ,p_data_vault_link 
                ,p_data_vault_link_sat
                );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-17 21:50:56
